package bizweb.test.elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GlobalPage {
	// Install	
			@FindBy(xpath="//a[contains(.,'Kho ứng dụng')]")
			public static WebElement btnAppInventory;
			
			@FindBy(xpath="//input[@placeholder='Bạn tìm ứng dụng gì?']")
			public static WebElement txtSearch;
			
			@FindBy(xpath="//div[@class='filter-form']//button")
			public static WebElement btnSearch;
			
			@FindBy(xpath="//div[@class='app-item']")
			public static WebElement itemApp;
			
			@FindBy(xpath="//div[@class='action']")
			public static WebElement btnSetup;
			
			@FindBy(xpath="//button[@id='btnLogin']")
			public static WebElement btnLogin;
			
			@FindBy(xpath="//input[@type='submit']")
			public static WebElement btnInstall;
			
			@FindBy(xpath="//span[@class='title']")
			public static WebElement titlePage;
			
		
		// Uninstall
			
			@FindBy(xpath="//a[@href='/admin/apps']")
			public static WebElement App;
			
			@FindBy(xpath="//div[@class='app']")
			public static WebElement appItem;
			
			@FindBy(xpath="//a[contains(.,'Thêm')]")
			public static WebElement btnMore;
			
			@FindBy(xpath="//a[contains(.,'Xóa ứng dụng')]")
			public static WebElement deleteApp;
			
			@FindBy(xpath="//button[contains(.,'Xóa')]")
			public static WebElement btnDelete;
			
			//@FindBy(xpath="//h1")
			@FindBy(xpath=".//*[@id='content']/div[3]/div[1]/h1")
			public static WebElement contentPage; 
			
		//Gmail
			@FindBy(xpath="//input[@id='identifierId']")
			public static WebElement emailGoogle;
			
			@FindBy(xpath="//span[contains(.,'Next')]")
			public static WebElement btnNext;
			
			@FindBy(xpath="//input[@name='password']")
			public static WebElement passGoogle;
			
			@FindBy(xpath="//b[contains(.,'Thông tin đăng nhập tài khoản')]")
			public static WebElement emailAccountTest;
			
			@FindBy(xpath="//div[@data-tooltip='Xóa']")
			public static WebElement iconDelete;
			
			@FindBy(xpath="//div[@data-tooltip='Chọn']")
			public static WebElement checkBox;
			
		//Create new product
			@FindBy(xpath="//a[@href='/admin/products']")
			public static WebElement menuProduct;
			
			@FindBy(xpath="//a[@href='/admin/products/create']")
			public static WebElement btnCreateNewProduct;
			
			@FindBy(xpath="//input[@name='Name']")
			public static WebElement txtNameProduct;
			
			@FindBy(xpath="//a[@bind-event-click='openFileUpload()']")
			public static WebElement linkAddImage;
			
			@FindBy(xpath="//input[@name='Variant.Price']")
			public static WebElement txtPriceProduct;
			
			@FindBy(xpath="//button[@class='btn btn-default btn-add']")
			public static WebElement btnSaveProduct;
			
		//Delete Order
			@FindBy(xpath="//nav//ul//li[2]//a")
			public static WebElement menuOrder;
			
			@FindBy(xpath="//nav//ul//li[2]//a")
			public static WebElement textSearchOrder;
			
			@FindBy(xpath="//td[2]//a")
			public static WebElement linkOrderDelete;
			
			@FindBy(xpath="//*[@id ='content']/div[1]/div[1]/div/div/a[2]")
			public static WebElement btnCancelOrder;
			
			@FindBy(xpath="//*[@id = 'bizweb-modal']//button[2]")
			public static WebElement btnCancelOrderPopup;
			
			@FindBy(xpath="//*[@id = 'content']/div[1]/div[5]//div//div//a")
			public static WebElement btnDeleteOrder;
			
			//Insert Order Checkout
			@FindBy(xpath="//a[contains(., 'Xem trên web')]")
			public static WebElement btnViewWebProduct;
			
			@FindBy(xpath="//button[@title = 'Thêm vào giỏ hàng']")
			public static WebElement btnAddToCart;
			
			@FindBy(xpath="//div[@class = 'cart-box']//span[1]")
			public static WebElement textCartBox;
			
			@FindBy(xpath="//button[@class='btn-checkout']")
			public static WebElement btnCheckout;
			
}
