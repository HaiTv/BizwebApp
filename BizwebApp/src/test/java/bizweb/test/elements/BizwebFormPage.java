package bizweb.test.elements;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BizwebFormPage {
	// Kết nối tài khoản 
		@FindBy(xpath = "//iframe[contains(@class,'app-iframe')]")
		public static WebElement iframe;
		
		@FindBy(xpath = "//*[@id='content']/div/div[2]/div/a[1]")
		public static WebElement btnConnectAcount;
		
		@FindBy(xpath = "//a[contains(.,'Kết nối Google Drive')]")
		public static WebElement btnConnectGoogleDrive;
		
		@FindBy(xpath = "//*[@id='identifierLink']/div[2]/p")
		public static WebElement btnUserAnotherAccount;
		
		
		@FindBy(xpath = "//*[@id='identifierNext']/content")
		public static WebElement btnNext1;
		
		@FindBy(xpath = "//*[@id='passwordNext']/content/span")
		public static WebElement btnNext2;
		
		@FindBy(xpath = "//*[@id='submit_approve_access']/content/span")
		public static WebElement btnAllow;
		
		@FindBy(xpath = "//div/div[1]/p/a")
		public static WebElement titleAccount;
		
		@FindBy(xpath = "//button[contains(.,'Đăng xuất')]")
		public static WebElement btnLogout;
		
		@FindBy(xpath = "//button[contains(.,'Chấp nhận')]")
		public static WebElement btnAccept;
		
		// go to List form page
		@FindBy(xpath = "//a[@href='/admin/apps/bizweb-form']")
		public static WebElement spanListFormpage;
		
		// go to List form page
		@FindBy(xpath = "//a[contains(.,'Danh sách form')]")
		public static WebElement btnListform;
		
		@FindBy(xpath = "//h3[contains(.,'Chưa')]")
		public static WebElement titleListSendEmpty;
		
		@FindBy(xpath = "//a[contains(.,'Quay lại')]")
		public static WebElement btnComeBack;
		
		// Tạo form
		@FindBy(xpath = "//a[contains(.,'Tạo form')]")
		public static WebElement btnCreateForm;
		
		@FindBy(xpath = "//a[contains(.,'Lưu')]")
		public static WebElement btnSaveForm;
		
		// kiểm tra thông báo
		@FindBy(xpath = "//*[@id='content']/div/div[3]/div/span")
		public static WebElement titleNotification;
		
		@FindBy(xpath = "//*[@id='Name']")
		public static WebElement txtNameForm;
		
		// kiểm tra tên form hiện thị
		@FindBy(xpath = "//*[@id='home-layout']/div[1]/div[1]/div/div/div/table/tbody/tr[1]/td[1]/a")
		public static WebElement titleNameForm;
		
		// sửa form
		@FindBy(xpath = "//a[@title='Chi tiết form']")
		public static WebElement iconEditFormPage;
		
		// sửa form
		@FindBy(xpath = "//a[contains(.,'Tiếp tục')]")
		public static WebElement btnContinue;
		
		// mã nhúng
		@FindBy(xpath = "//a[@title='Mã nhúng']")
		public static WebElement iconEmbedcode;
		
		@FindBy(xpath = "//button[@id='btnCopy']")
		public static WebElement btnCopyEmbedcode;
		
		@FindBy(xpath = "//button[contains(.,'Đóng')]")
		public static WebElement btnCloseEmbedcode;
		
		
		// copy mã nhúng vào templace
		@FindBy(xpath = "//a[contains(.,'Website')]")
		public static WebElement itemWebsite;
		
		@FindBy(xpath = "//a[@href='/admin/themes']")
		public static WebElement itemTheme;
		
		@FindBy(xpath = "//*[@id='content']/div/form/div[3]/div/div/div[1]/div/div[2]/span/a")
		public static WebElement iconMore;
		
		@FindBy(xpath = "//a[contains(.,'Chỉnh sửa HTML/CSS')]")
		public static WebElement itemEditHtmlCss;
		
		@FindBy(xpath = "//a[contains(.,'blog.bwt')]")
		public static WebElement itemBlog;
		
		@FindBy(xpath = "//*[@id='code-asset-container']/div/div[6]/div[1]/div/div/div/div[5]/div[1]/pre")
		public static WebElement txtPasteEmbedcode;
		
//		@FindBy(xpath = "//tr[1]//td[1]//a")
//		public static WebElement txtCutString;
		
		@FindBy(xpath = "//a[@id='save-button']")
		public static WebElement btnSaveTheme;
		
		// hiện thị website
		@FindBy(xpath = "//a[@title='Xem website của bạn']")
		public static WebElement iconViewWebsite;
		
		@FindBy(xpath = "//*[@id='nav']/li[3]/a")
		public static WebElement itemViewBlogPage;
		
		@FindBy(xpath = "//div[@data-app='bizweb_form']//iframe")
		public static WebElement iframeFrontend;
		
		@FindBy(xpath = "//div[1]/div/h3")
		public static WebElement titleNameFormFrontend;
		
		@FindBy(xpath = "//*[@id='btnSubmit']")
		public static WebElement btnSubmitFrontend;
		
		@FindBy(xpath = "//*[@id='Data_0__Result-error']")
		public static WebElement titleErrorFrontend;
		
		//Danh sách gửi
		
		@FindBy(xpath = "//tr[1]/td[3]")
		public static WebElement titleTimeSend;
		
		@FindBy(xpath = "//tr[1]/td[4]/a[2]/i")
		public static WebElement iconListSend;
		
		@FindBy(xpath = "//tr[1]/td[7]/a[1]/i")
		public static WebElement iconViewDetail;
		
		@FindBy(xpath = "//tbody/tr[1]/td[2]")
		public static WebElement titleContentList;
		
		@FindBy(xpath = "//span[@class='title']")
		public static WebElement titleContent;
		
		@FindBy(xpath = "//*[@id='content']/div/div[2]/div/a")
		public static WebElement btnBack;
		
		@FindBy(xpath = "//tbody/tr[1]/td[7]/a[2]/i")
		public static WebElement iconDeleteListSend;
		
		@FindBy(xpath = "//*[@id='myModalDelForm']/div/div/form/div[3]/input")
		public static WebElement btnAcceptDelete;
		
		
		//xóa form
		@FindBy(xpath = "//a[@title='Xóa form']")
		public static WebElement iconDeleteForm;
		
		@FindBy(xpath = "//iframe")
		public static WebElement iframeDeleteform;
		
		@FindBy(xpath = "//input[@value='Xóa']")
		public static WebElement btnDeleteForm;
		
		//kéo trường thông tin 
		@FindBy(xpath = "//*[@id='div-module-form']/div[1]/div[1]/div[2]/h4")
		public static WebElement titleTextField;
		
		@FindBy(xpath = "//div[@id='FormManagement']//div[1]//a[@title='Chỉnh sửa']")
		public static WebElement iconEditTextField;
		
		@FindBy(xpath = "//*[@id='cbDocRequired']")
		public static WebElement ckeckboxRequiredStateTextField;
		
		@FindBy(xpath = "//*[@id='hdType1']/div/input[2]")
		public static WebElement btnSaveTextField;
		
		
		@FindBy(xpath = "//*[@id='div-module-form']/div[1]/div[2]/div[2]/h4")
		public static WebElement titleEnterNumber;
		
		@FindBy(xpath = "//div[@id='FormManagement']//div[2]//a[@title='Chỉnh sửa']")
		public static WebElement iconEditEnterNumber;
		
		@FindBy(xpath = "//*[@id='cbNumberRequired']")
		public static WebElement ckeckboxRequiredStateEnterNumber;
		
		@FindBy(xpath = "//*[@id='hdType2']/div/input[2]")
		public static WebElement btnSaveEnterNumber;
		
		@FindBy(xpath = "//*[@id='div-module-form']/div[1]/div[3]/div[2]/h4")
		public static WebElement titleEmail;
		
		@FindBy(xpath = "//div[@id='FormManagement']//div[3]//a[@title='Chỉnh sửa']")
		public static WebElement iconEditEmail;
		
		@FindBy(xpath = "//*[@id='cbEmailRequired']")
		public static WebElement ckeckboxRequiredStateEmail;
		
		@FindBy(xpath = "//*[@id='hdType11']/div/input[2]")
		public static WebElement btnSaveEmail;
		
		@FindBy(xpath = "//*[@id='div-module-form']/div[1]/div[4]/div[2]/h4")
		public static WebElement titleEnterPhone;
		
		@FindBy(xpath = "//div[@id='FormManagement']//div[4]//a[@title='Chỉnh sửa']")
		public static WebElement iconEditEnterPhone;
		
		@FindBy(xpath = "//*[@id='cbNumberTextRequired']")
		public static WebElement ckeckboxRequiredStateEnterPhone;
		
		@FindBy(xpath = "//*[@id='hdType12']/div/input[2]")
		public static WebElement btnSaveEnterPhone;
		
		@FindBy(xpath = "//*[@id='div-module-form']/div[1]/div[5]/div[2]/h4")
		public static WebElement titleNation;
		
		@FindBy(xpath = "//div[@id='FormManagement']//div[5]//a[@title='Chỉnh sửa']")
		public static WebElement iconEditNation;
		
		@FindBy(xpath = "//*[@id='cbRegionRequired']")
		public static WebElement ckeckboxRequiredStateNation;
		
		@FindBy(xpath = "//*[@id='hdType6']/div/input[2]")
		public static WebElement btnSaveNation;
		
		@FindBy(xpath = "//*[@id='div-module-form']/div[1]/div[6]/div[2]/h4")
		public static WebElement titleMultiLineText;
		
		@FindBy(xpath = "//div[@id='FormManagement']//div[6]//a[@title='Chỉnh sửa']")
		public static WebElement iconEditMultiLineText;
		
		@FindBy(xpath = "//*[@id='cbMultiRequired']")
		public static WebElement ckeckboxRequiredStateMultiLineText;
		
		@FindBy(xpath = "//*[@id='hdType10']/div/input[2]")
		public static WebElement btnSaveMultiLineText;
		
		@FindBy(xpath = "//*[@id='div-module-form']/div[1]/div[7]/div[2]/h4")
		public static WebElement titleManyChoices;
		
		@FindBy(xpath = "//div[@id='FormManagement']//div[7]//a[@title='Chỉnh sửa']")
		public static WebElement iconEditManyChoices;
		
		@FindBy(xpath = "//*[@id='cbCheckboxRequired']")
		public static WebElement ckeckboxRequiredStateManyChoices;
		
		@FindBy(xpath = "//*[@id='hdType4']/div/input[2]")
		public static WebElement btnSaveManyChoices;
		
		@FindBy(xpath = "//*[@id='div-module-form']/div[1]/div[8]/div[2]/h4")
		public static WebElement titleChooseOne;
		
		@FindBy(xpath = "//div[@id='FormManagement']//div[8]//a[@title='Chỉnh sửa']")
		public static WebElement iconEditChooseOne;
		
		@FindBy(xpath = "//*[@id='cbRadioRequired']")
		public static WebElement ckeckboxRequiredStateChooseOne;
		
		@FindBy(xpath = "//*[@id='hdType3']/div/input[2]")
		public static WebElement btnSaveChooseOne;
		
		@FindBy(xpath = "//*[@id='div-module-form']/div[1]/div[9]/div[2]/h4")
		public static WebElement titleDroplistSelection;
		
		@FindBy(xpath = "//div[@id='FormManagement']//div[9]//a[@title='Chỉnh sửa']")
		public static WebElement iconEditDroplistSelection;
		
		@FindBy(xpath = "//*[@id='cbDDLRequired']")
		public static WebElement ckeckboxRequiredStateDroplistSelection;
		
		@FindBy(xpath = "//*[@id='hdType5']/div/input[2]")
		public static WebElement btnSaveDroplistSelection;
		
		@FindBy(xpath = "//*[@id='div-module-form']/div[1]/div[10]/div[2]/h4")
		public static WebElement titleDate;
		
		@FindBy(xpath = "//div[@id='FormManagement']//div[10]//a[@title='Chỉnh sửa']")
		public static WebElement iconEditDate;
		
		@FindBy(xpath = "//*[@id='cbDateRequired']")
		public static WebElement ckeckboxRequiredStateDate;
		
		@FindBy(xpath = "//*[@id='hdType7']/div/input[2]")
		public static WebElement btnSaveDate;
		
		@FindBy(xpath = "//*[@id='div-module-form']/div[1]/div[11]/div[2]/h4")
		public static WebElement titleTime;
		
		@FindBy(xpath = "//div[@id='FormManagement']//div[11]//a[@title='Chỉnh sửa']")
		public static WebElement iconEditTime;
		
		@FindBy(xpath = "//*[@id='cbTimeRequired']")
		public static WebElement ckeckboxRequiredStateTime;
		
		@FindBy(xpath = "//*[@id='hdType8']/div/input[2]")
		public static WebElement btnSaveTime;
		
		@FindBy(xpath = "//*[@id='div-module-form']/div[1]/div[12]/div[2]/h4")
		public static WebElement titleColor;
		
		@FindBy(xpath = "//div[@id='FormManagement']//div[12]//a[@title='Chỉnh sửa']")
		public static WebElement iconEditColor;
		
		@FindBy(xpath = "//*[@id='cbColorRequired']")
		public static WebElement ckeckboxRequiredStateColor;
		
		@FindBy(xpath = "//*[@id='hdType9']/div/input[2]")
		public static WebElement btnSaveColor;
//		
//		@FindBy(xpath = "//*[@id='div-module-form']/div[1]/div[13]/div[2]/h4")
//		public static WebElement uploadfile;
		
		@FindBy(xpath = "//*[@id='FormManagement']")
		public static WebElement FormManagement;
		
		@FindBy(xpath = "//*[@id='CaptchaTitle']")
		public static WebElement txtCaptcha;
		
		@FindBy(xpath = "//*[@id='SendButtonTitle']")
		public static WebElement txtSendButton;
		
		@FindBy(xpath = "//*[@id='ResetButtonTitle']")
		public static WebElement txtResetButton;
		
		@FindBy(xpath = "//*[@id='WidthDesktop']")
		public static WebElement txtWidthDesktop;
		
		@FindBy(xpath = "//*[@id='HiddenTitle']")
		public static WebElement checkboxHiddenTitle;
		
		//nhập thông tin ở frontend
		@FindBy(xpath = "//*[@id='Data_0__Result']")
		public static WebElement txtEnterText;
		
		@FindBy(xpath = "//*[@id='Data_1__Result']")
		public static WebElement txtEnterNumber;
		
		@FindBy(xpath = "//*[@id='Data_2__Result']")
		public static WebElement txtEnterEmail;
		
		@FindBy(xpath = "//*[@id='Data_3__Result']")
		public static WebElement txtEnterPhone;
		
		@FindBy(xpath = "//*[@id='Data_5__Result']")
		public static WebElement txtMultiLineText;
		
		@FindBy(xpath = "//div[@class='checkbox']/label/input[@value='Lựa chọn 1']")
		public static WebElement checkboxManyChoices;
		
		@FindBy(xpath = "//div[@class='radio']/label/input[@value='Lựa chọn 1']")
		public static WebElement radioChooseOne;
		
		@FindBy(xpath = "//*[@id='Data_8__Result']")
		public static WebElement selectDroplistSelection;
		
		@FindBy(xpath = "//*[@id='Data_9__Result']")
		public static WebElement txtDate;
		
		@FindBy(xpath = "//*[@id='Data_10__Result']")
		public static WebElement txtTime;
		
		@FindBy(xpath = "//*[@id='Data_11__Result']")
		public static WebElement txtColor;
		
		@FindBy(xpath = "//div[1]/div[2]/div/p")
		public static WebElement titleMessageSendform;
		
		//Thông báo phản hồi
		@FindBy(xpath = "//iframe[@title='Bộ soạn thảo văn bản có định dạng, CompletedMessage']")
		public static WebElement iframeMessage;
		
		@FindBy(xpath = "html/body")
		public static WebElement txtEnterMassage;
		
		@FindBy(xpath = "//*[@id='ddlIsResponse']")
		public static WebElement selectboxFeedback;
		
		@FindBy(xpath = "//*[@id='sl_SentEmail']")
		public static WebElement selectboxEmail;
		
		@FindBy(xpath = "//*[@id='ResponseSubject']")
		public static WebElement titleEmailFeedback;
		
		@FindBy(xpath = "//iframe[@title='Bộ soạn thảo văn bản có định dạng, ResponseContent']")
		public static WebElement iframeContentEmail;
		
		//Gmail
		@FindBy(xpath = "//a[contains(.,'Sign In')]")
		public static WebElement btnSigninGmail;
		
		@FindBy(xpath = "//span[contains(.,'Tiêu')]")
		public static WebElement titleGmail;
		
		@FindBy(xpath = "//*[@id=':5']/div[2]/div[1]/div/div[2]/div[3]/div/div")
		public static WebElement btnDeleteMail;
		
		
		//Logout Account at GoogleDrive
		
		@FindBy(xpath = "//*[@id='gb']/div[2]/div[3]/div/div[3]/div/a")
		public static WebElement iconAccount;
		
		@FindBy(xpath = "//a[contains(.,'Đăng xuất')]")
		public static WebElement btnLogoutAccount;
		
		@FindBy(xpath = "//*[@id='headingText']")
		public static WebElement titleNextAccount;
		
		
		
}
