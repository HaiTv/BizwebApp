package bizweb.test.actions;

import static com.jayway.restassured.RestAssured.given;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverAction {
	public static WebDriver driver;
	public WebDriverWait wait = new WebDriverWait (driver, 60);
	public JavascriptExecutor ex = (JavascriptExecutor) driver;
	
	public void open(String url){
		driver.get(url);
	}
	
	public void close(){
		driver.close();
	}
	
	public void refresh(){
		driver.navigate().refresh();
		if(isAlertPresent()==true){
			driver.switchTo().alert().dismiss();
		}
	}

	public void click(WebElement element){
		Actions actions = new Actions(driver);
		actions.moveToElement(element).click().perform();
	}
	
	public void doubleClick(WebElement element){
		Actions action = new Actions(driver);
		action.doubleClick(element).build().perform();
	}
	
	public void clickToPoint(WebElement element){
		JavascriptExecutor ex = (JavascriptExecutor)driver;
		ex.executeScript("arguments[0].click();", element);
	}
	
	public void clear(WebElement element){
		element.clear();
	}
	
	public void sendKeys(WebElement element, String value){
		element.sendKeys(value);
	}
	
	public void sendKeysWithIframe(WebElement iframe, WebElement body, String value){
		driver.switchTo().frame(iframe);
		  body.click();
		  body.clear();
		  body.sendKeys(value);
		driver.switchTo().defaultContent();
	}
	
	public void switchToIframe(WebElement iframe){
		driver.switchTo().frame(iframe);
	}
	
	public void exitIframe(){
		driver.switchTo().defaultContent();
	}
	
	public void scroll(int x, int y){
		JavascriptExecutor jse = (JavascriptExecutor)driver;
//		jse.executeScript("window.scrollBy("+ x + ", " + y + ")", "");
		jse.executeScript("scroll("+ x + ", " + y + ");");
	}
	
	public void Screen(String name) throws IOException {
		DateFormat dateFormat = new SimpleDateFormat("HH/mm");
		Date date = new Date();
		String scrDate = dateFormat.format(date);
		TakesScreenshot ts = (TakesScreenshot)driver;
		File scrFile = ts.getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		FileUtils.copyFile(scrFile, new File(Const.ScreenShot+name+scrDate+".png"));
		
	}
	
	public String getText(WebElement element){
		return element.getText();
	}
	
	public void assertText(WebElement element, String text){
		Assert.assertEquals(text, element.getText());
	}
	
	public void compareString(String actual, String expected){
		Assert.assertTrue(actual.equals(expected));
	}
	
	public void checkElementContainsText(WebElement element, String text){
		String actualString = element.getText();
		Assert.assertTrue(actualString.contains(text));
	}
	
	public String getValueOfAttribute(WebElement element, String attribute){
		String value = element.getAttribute(attribute);
		return value;
	}
	
	public void assertValueOfAttribute(WebElement element, String attribute, String text){
		String value = element.getAttribute(attribute);		
		Assert.assertEquals(text, value);
	}
	
	public void hoverAndClick(WebElement hover, WebElement target){
		Actions action = new Actions(driver);
		action.moveToElement(hover).moveToElement(target).click().build().perform();
	}
	
	public void selectDropbox(WebElement element, String text){
		Select select = new Select(element);
		select.selectByVisibleText(text);
	}
	
	public void selectDropbox(WebElement element, int index){
		Select select = new Select(element);
		select.selectByIndex(index);
	}
	
	public void dragAndDrop(WebElement src, WebElement targ){
		Actions action = new Actions(driver);
		Action dragDrop = action.dragAndDrop(src, targ).build();
//		Action dragDrop = action.dragAndDropBy(src, 0 , -100).build();
		dragDrop.perform();
	}
	
	public void checkBox(WebElement element){
		try{
		if (!element.isSelected())
		{
		     element.click();
		}
		}
		catch (Exception e)
		{
			System.out.println("Checkbox was Selected!");
		}
	}
	
	public void unCheckBox(WebElement element){
		try{
			if (element.isSelected())
			{
			    element.click();
			}
		}
		catch(Exception e)
		{
			System.out.println("Checkbox was UnChecked!");
		}
	}
	
	public void waitElement(WebElement element){
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	public void waitElementToClick(WebElement element){
		wait.until(ExpectedConditions.elementToBeClickable(element)).click();
	}
	
	public void waitElementToSendKeys(WebElement element, String value){
		wait.until(ExpectedConditions.elementToBeClickable(element)).sendKeys(value);;
	}
	
	public void waitElementWithText(String locator, String text){
		wait.until(ExpectedConditions.invisibilityOfElementWithText(By.xpath(locator), driver.findElement(By.xpath(locator)).getText()));
		Assert.assertEquals(driver.findElement(By.xpath(locator)).getText(), text);
	}
	
	public void waitElementWithText(WebElement element, String text){
		wait.until(ExpectedConditions.visibilityOf(element));
		String expected_text = element.getText();
		Assert.assertEquals(expected_text, text);
	}
	
	public boolean isAlertPresent() 
	{ 
	    try 
	    { 
	        driver.switchTo().alert(); 
	        return true; 
	    }
	    catch (Exception e) 
	    { 
	        return false; 
	    }
	}	
	
	public void refreshForWaitElement(final WebElement element){
		ExpectedCondition <Boolean> example = new ExpectedCondition <Boolean>(){ 
			public Boolean apply(WebDriver args){
				driver.navigate().refresh();
				if(isAlertPresent()==true){
					driver.switchTo().alert().dismiss();
				}
				return element.isEnabled();
			}
		};
		wait.until(example);
	}
	
	public void refreshForWaitElementInIframe(final WebElement element, final WebElement iframe){
		  ExpectedCondition <Boolean> example = new ExpectedCondition <Boolean>(){
		   public Boolean apply(WebDriver args){
		    driver.navigate().refresh();
		    if(isAlertPresent()==true){
				driver.switchTo().alert().dismiss();
			}
		    driver.switchTo().frame(iframe);
		    return element.isEnabled();
		   }
		  };
		  wait.until(example);
	}
	
	public void refreshForWaitElementInIframeNoDisplay(final WebElement element, final WebElement iframe){
		ExpectedCondition <Boolean> example = new ExpectedCondition <Boolean>(){ 
			public Boolean apply(WebDriver args){
				driver.navigate().refresh();
				if(isAlertPresent()==true){
					driver.switchTo().alert().dismiss();
				}
				driver.switchTo().frame(iframe);
				return !(element.isEnabled());
			}
		};
		wait.until(example);
	}
	
	public void isEnabled(WebElement element){
		Assert.assertEquals(true, element.isEnabled());
	}
	
	public void isDisplayed(WebElement element){
		Assert.assertEquals(true, element.isDisplayed());
	}
	
	public void waitElementIsInvisible(String locator){
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
	}
	
//	public void isDisabled(WebElement element){
//		Assert.assertEquals(false, element.isEnabled());
//	}
	public void openNewTabWindow(String url){
		((JavascriptExecutor)driver).executeScript("window.open()");		
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());   
		   driver.switchTo().window(tabs.get(tabs.size()-1)); 
		   driver.get(url);
	}
	
	public void closeTabWindow(String title){	   
		   Set<String> allWindows = driver.getWindowHandles();
		   if(!allWindows.isEmpty()) {
		   for (String windowId : allWindows){
		   try {
		   if(driver.switchTo().window(windowId).getTitle().equals(title)) 
		   {
		   driver.close();
		   break;
		   } }
		   catch(NoSuchWindowException e) {
		   e.printStackTrace();
		   } } }
	}
	
	public void closeTabWindow(int index){
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());   
		   try {
		   if(driver.switchTo().window(tabs.get(index)) != null) 
		   {
			   driver.close();
		   } }
		   catch(NoSuchWindowException e) {
		   e.printStackTrace();
		   }
	}
	
	public void switchTabWindow(int tab_number){
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());   
		   try {
			   driver.switchTo().window(tabs.get(tab_number));
			   }
		   catch(NoSuchWindowException e) {
		   e.printStackTrace();
		   }
	}
	
	public void uploadFile(String path){
		try{
		StringSelection string = new StringSelection(path);
	      Toolkit.getDefaultToolkit().getSystemClipboard().setContents(string, null);
	      
	      Thread.sleep(5000);
	      Robot robot = new Robot();
	      robot.keyPress(KeyEvent.VK_CONTROL);
	      robot.keyPress(KeyEvent.VK_V);
	      robot.keyRelease(KeyEvent.VK_V);
	      robot.keyRelease(KeyEvent.VK_CONTROL);
	      robot.keyPress(KeyEvent.VK_ENTER);
	      robot.keyRelease(KeyEvent.VK_ENTER);
		}
		catch(Exception e)
		{
			System.out.println("CÓ LỖI KHI UPLOAD FILE" + e.getMessage());
		}
	}
	
//	public void uploadFile(String path, String idInput){
//		  checkElementExist(By.xpath(""+ idInput +""));
//		  if(driver.findElement(By.xpath(""+ idInput +"")).getAttribute("style") == "none")
//		   ex.executeScript("document.getElementByXpath('"+idInput+"').style.display = 'block';");
//		  driver.findElement(By.xpath(""+idInput+"")).sendKeys(path);
//		 }
	
	public void verifyPrintDialog(){
		driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
		((JavascriptExecutor)driver).executeScript(
			    "var callback = arguments[1];" +
			    "window.print = function(){callback();};");	
	}
	
	public void clickPrintAndVerifyPrintDialog(WebElement element){
		driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
		((JavascriptExecutor)driver).executeScript(
			    "var callback = arguments[1];" +
			    "window.print = function(){callback();};" +
			    "arguments[0].click();"
			    , element);
	}
	
	public void closePrintDialog() throws InterruptedException{
		Thread.sleep(5000);
		Robot r;
		try {
			r = new Robot();
			r.keyPress(KeyEvent.VK_ESCAPE);
			r.keyRelease(KeyEvent.VK_ESCAPE);
		} catch (AWTException e) {
			e.printStackTrace();
		}
		catch (Exception e){
			System.out.println("Không đóng được Dialog!");
		}
	}
	
//	public void printDialog(){
//		Robot r;
//		try {
//			r = new Robot();
//			r.keyPress(KeyEvent.VK_ENTER) ;
//	        r.keyRelease(KeyEvent.VK_ENTER) ;
//		} catch (AWTException e) {
//			e.printStackTrace();
//		}
//	}
	
	public void waitForPageLoad() 
    {
        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return String
                    .valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
                    .equals("complete");
            }
        };
        wait.until(expectation);
    }
	
	public int getStatusCode(){
        String urlCurrent = driver.getCurrentUrl().toString();
        return given().when().baseUri(urlCurrent).then().get().getStatusCode();
    }
	
	public Boolean checkElementExist(final By by) 
	{
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {				
				if(!driver.findElements(by).isEmpty())
					   return true;
				   else
					   return false;
			}
		};
		return wait.until(expectation);
	}
	
	public void clickElementWithIframe(WebElement iframe, WebElement element){
        driver.switchTo().frame(iframe);
        element.click();
        driver.switchTo().defaultContent();
	}
}
