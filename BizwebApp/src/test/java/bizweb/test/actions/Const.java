package bizweb.test.actions;

public class Const {
	public static final String URL_ADMIN = "https://ventomarme90.bizwebvietnam.net/admin";
	public static final String EMAIL = "testerbizweb@gmail.com";
	public static final String PASS = "123456";
	public static final String PASSGMAIL = "testerbizweb@123";
	public static final String PATH = ".\\src\\test\\resources\\Libs\\chromedriver.exe";
	public static final String ScreenShot = ".\\src\\test\\resources\\ScreenShot\\";
	public static final String NOTIF_LOCATION = "//div[@class = 'ajax-notification is-visible']//span";
	
}
