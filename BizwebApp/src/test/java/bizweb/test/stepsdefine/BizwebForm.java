package bizweb.test.stepsdefine;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;

import bizweb.test.actions.Base;
import bizweb.test.elements.BizwebFormPage;
import bizweb.test.elements.GlobalPage;
import bizweb.test.elements.LoginPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BizwebForm extends Base {
	String value = "Form 1";
	String updatevalue = "Form 2";
	String enterVanban = "Trường Văn Bản";
	String enterNumber = "1234";
	String enterPhone = "0967456354";
	String enterEmail = "haibiboy1995@gmail.com";

	public BizwebForm() {
		PageFactory.initElements(driver, BizwebFormPage.class);
	}

	@When("^I install app success$")
	public void i_install_app_success() throws Throwable {
		installApp("Biểu mẫu khảo sát");
	}

	@Then("^I see form display in app page$")
	public void i_see_form_display_in_app_page() throws Throwable {
		assertText(GlobalPage.titlePage, "Danh sách form");
	}

	@Given("^I am on list form page$")
	public void i_am_on_List_form_Page() throws Throwable {
		// switchTabWindow(1);
		waitElementToClick(BizwebFormPage.spanListFormpage);
	}

	@When("^I access create form page$")
	public void i_access_Create_form_page() throws Throwable {
		waitElementToClick(BizwebFormPage.btnCreateForm);
	}

	@When("^I perform create form unsuccess$")
	public void create_form_Unsuccess() throws Throwable {
		waitElementToClick(BizwebFormPage.btnSaveForm);
	}

	@Then("^I see message error with create form$")
	public void i_see_message_error_with_create_form() throws Throwable {
		waitElementWithText(BizwebFormPage.titleNotification, "Vui lòng kiểm tra lại các trường dữ liệu!");
	}

	@When("^I perform input data$")
	public void input_Data() throws Throwable {
		refresh();
		switchToIframe(BizwebFormPage.iframe);
		waitElement(BizwebFormPage.txtNameForm);
		sendKeys(BizwebFormPage.txtNameForm, value);
		exitIframe();
		waitElementToClick(BizwebFormPage.btnSaveForm);
	}

	@Then("^I see message create success$")
	public void i_see_message_success_when_enter_name() throws Throwable {
		waitElementWithText(BizwebFormPage.titleNotification, "Tạo form thành công!");
	}

	@Then("^I see form display in from list when create$")
	public void form_Display_in_from_list_when_create() throws Throwable {
		click(BizwebFormPage.btnListform);
		switchToIframe(BizwebFormPage.iframe);
		assertText(BizwebFormPage.titleNameForm, value);
	}

	@When("^I update form success when enter new name$")
	public void update_form_Success_when_enter_new_name() throws Throwable {
		waitElementToClick(BizwebFormPage.iconEditFormPage);
		waitElement(BizwebFormPage.txtNameForm);
		clear(BizwebFormPage.txtNameForm);
		sendKeys(BizwebFormPage.txtNameForm, updatevalue);
		exitIframe();
		Thread.sleep(2000);
		waitElementToClick(BizwebFormPage.btnSaveForm);
	}

	@Then("^I see message update success$")
	public void i_see_message_success() throws Throwable {
		waitElementWithText(BizwebFormPage.titleNotification, "Cập nhật thành công!");
	}

	@Then("^I see form display in from list when update$")
	public void form_Display_in_from_list_when_update() throws Throwable {
		Thread.sleep(1200);
		click(BizwebFormPage.btnListform);
		switchToIframe(BizwebFormPage.iframe);
		assertText(BizwebFormPage.titleNameForm, updatevalue);
	}

	@When("^I click icon embedded code$")
	public void i_Click_icon_Embedded_code() throws Throwable {
		waitElementToClick(BizwebFormPage.iconEmbedcode);
		Thread.sleep(2000);
		waitElementToClick(BizwebFormPage.btnCopyEmbedcode);
		waitElementToClick(BizwebFormPage.btnCloseEmbedcode);

	}

	@When("^I copy embedded code and paste in templace$")
	public void i_copy_Embedded_code_and_paste_in_Templace() throws Throwable {
		// String href = getValueOfAttribute(AppBieumauPage.txtCutString, "href");
		// if (href.startsWith("https://bizwebform.bizwebapps.vn/bizwebform/EditForm/"))
		// {
		// href = href.substring(53);
		// }
		// System.out.println(href);
		exitIframe();
		waitElementToClick(BizwebFormPage.itemWebsite);
		waitElement(BizwebFormPage.itemTheme);
		waitElementToClick(BizwebFormPage.itemTheme);
		waitElementToClick(BizwebFormPage.iconMore);
		waitElementToClick(BizwebFormPage.itemEditHtmlCss);
		waitElementToClick(BizwebFormPage.itemBlog);
		// Thread.sleep(2000);
		waitElement(BizwebFormPage.txtPasteEmbedcode);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(2000);
		// Actions action = new Actions(driver);
		// action.moveToElement(AppBieumauPage.copymanhung).sendKeys(AppBieumauPage.copymanhung,"<div
		// data-app='bizweb_form' data-form-id='"+href+"'></").build().perform();
		waitElementToClick(BizwebFormPage.btnSaveTheme);
	}

	@When("^I click display frontend page$")
	public void display_Frontend_Page() throws Throwable {
		click(BizwebFormPage.iconViewWebsite);
		Thread.sleep(2000);
		switchTabWindow(2);
		waitElementToClick(BizwebFormPage.itemViewBlogPage);
	}

	@Then("^I do not see form display in frontend$")
	public void i_do_not_see_form_display_in_frontend() throws Throwable {
		Thread.sleep(3000);

	}

	@When("^I access list form page$")
	public void i_access_List_form_page() throws Throwable {
		closeTabWindow(2);
		switchTabWindow(1);
		waitElementToClick(BizwebFormPage.txtPasteEmbedcode);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_BACK_SPACE);
		robot.keyRelease(KeyEvent.VK_BACK_SPACE);
		robot.keyPress(KeyEvent.VK_DELETE);
		robot.keyRelease(KeyEvent.VK_DELETE);
		waitElementToClick(BizwebFormPage.btnSaveTheme);
		Thread.sleep(2000);
		waitElement(LoginPage.menuApps);
		Thread.sleep(1000);
		clickToPoint(LoginPage.menuApps);
		waitElementToClick(BizwebFormPage.spanListFormpage);
	}

	@When("^I access list send form$")
	public void i_access_list_send_form() throws Throwable {
		switchToIframe(BizwebFormPage.iframe);
		waitElement(BizwebFormPage.iconListSend);
		click(BizwebFormPage.iconListSend);
	}

	@Then("^I see list send form empty$")
	public void i_see_List_send_form_empty() throws Throwable {
		waitElementWithText(BizwebFormPage.titleListSendEmpty, "Chưa có dữ liệu");
	}

	@When("^I perform back list form$")
	public void back_List_form() throws Throwable {
		exitIframe();
		waitElementToClick(BizwebFormPage.btnComeBack);
	}

	@When("^I perform a delete form$")
	public void i_Perform_a_delete_form() throws Throwable {
		switchToIframe(BizwebFormPage.iframe);
		click(BizwebFormPage.iconDeleteForm);
		Thread.sleep(2000);
		click(BizwebFormPage.btnDeleteForm);
		exitIframe();
	}

	@Then("^I see display message delete form$")
	public void i_see_Display_Message_delete_form() throws Throwable {

	}

	@When("^I perform create form success with pull all information feild$")
	public void create_form_Success_with_pull_all_information_feild() throws Throwable {
		refresh();
		switchToIframe(BizwebFormPage.iframe);
		waitElement(BizwebFormPage.txtNameForm);
		sendKeys(BizwebFormPage.txtNameForm, value);
		Thread.sleep(2000);
		waitElement(BizwebFormPage.btnContinue);
		click(BizwebFormPage.btnContinue);
		Thread.sleep(2000);
		dragAndDrop(BizwebFormPage.titleTextField, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleEnterNumber, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleEmail, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleEnterPhone, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleNation, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleMultiLineText, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleManyChoices, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleChooseOne, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleDroplistSelection, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleDate, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleTime, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleColor, BizwebFormPage.FormManagement);
		exitIframe();
		waitElementToClick(BizwebFormPage.btnSaveForm);

	}

	@When("^I update form success when choose required state$")
	public void i_Update_form_Success() throws Throwable {
		waitElementToClick(BizwebFormPage.iconEditFormPage);
		waitElement(BizwebFormPage.txtNameForm);
		clear(BizwebFormPage.txtNameForm);
		sendKeys(BizwebFormPage.txtNameForm, updatevalue);
		waitElement(BizwebFormPage.btnContinue);
		click(BizwebFormPage.btnContinue);
		Thread.sleep(2000);
		clickToPoint(BizwebFormPage.iconEditTextField);
		waitElement(BizwebFormPage.ckeckboxRequiredStateTextField);
		checkBox(BizwebFormPage.ckeckboxRequiredStateTextField);
		waitElementToClick(BizwebFormPage.btnSaveTextField);
		clickToPoint(BizwebFormPage.iconEditEnterNumber);
		waitElement(BizwebFormPage.ckeckboxRequiredStateEnterNumber);
		checkBox(BizwebFormPage.ckeckboxRequiredStateEnterNumber);
		waitElementToClick(BizwebFormPage.btnSaveEnterNumber);
		clickToPoint(BizwebFormPage.iconEditEmail);
		waitElement(BizwebFormPage.ckeckboxRequiredStateEmail);
		checkBox(BizwebFormPage.ckeckboxRequiredStateEmail);
		waitElementToClick(BizwebFormPage.btnSaveEmail);
		clickToPoint(BizwebFormPage.iconEditEnterPhone);
		waitElement(BizwebFormPage.ckeckboxRequiredStateEnterPhone);
		checkBox(BizwebFormPage.ckeckboxRequiredStateEnterPhone);
		waitElementToClick(BizwebFormPage.btnSaveEnterPhone);
		clickToPoint(BizwebFormPage.iconEditNation);
		waitElement(BizwebFormPage.ckeckboxRequiredStateNation);
		checkBox(BizwebFormPage.ckeckboxRequiredStateNation);
		waitElementToClick(BizwebFormPage.btnSaveNation);
		clickToPoint(BizwebFormPage.iconEditMultiLineText);
		waitElement(BizwebFormPage.ckeckboxRequiredStateMultiLineText);
		checkBox(BizwebFormPage.ckeckboxRequiredStateMultiLineText);
		waitElementToClick(BizwebFormPage.btnSaveMultiLineText);
		clickToPoint(BizwebFormPage.iconEditManyChoices);
		waitElement(BizwebFormPage.ckeckboxRequiredStateManyChoices);
		checkBox(BizwebFormPage.ckeckboxRequiredStateManyChoices);
		waitElementToClick(BizwebFormPage.btnSaveManyChoices);
		clickToPoint(BizwebFormPage.iconEditChooseOne);
		waitElement(BizwebFormPage.ckeckboxRequiredStateChooseOne);
		checkBox(BizwebFormPage.ckeckboxRequiredStateChooseOne);
		waitElementToClick(BizwebFormPage.btnSaveChooseOne);
		clickToPoint(BizwebFormPage.iconEditDroplistSelection);
		waitElement(BizwebFormPage.ckeckboxRequiredStateDroplistSelection);
		checkBox(BizwebFormPage.ckeckboxRequiredStateDroplistSelection);
		waitElementToClick(BizwebFormPage.btnSaveDroplistSelection);
		clickToPoint(BizwebFormPage.iconEditDate);
		waitElement(BizwebFormPage.ckeckboxRequiredStateDate);
		checkBox(BizwebFormPage.ckeckboxRequiredStateDate);
		waitElementToClick(BizwebFormPage.btnSaveDate);
		clickToPoint(BizwebFormPage.iconEditTime);
		waitElement(BizwebFormPage.ckeckboxRequiredStateTime);
		checkBox(BizwebFormPage.ckeckboxRequiredStateTime);
		waitElementToClick(BizwebFormPage.btnSaveTime);
		clickToPoint(BizwebFormPage.iconEditColor);
		waitElement(BizwebFormPage.ckeckboxRequiredStateColor);
		checkBox(BizwebFormPage.ckeckboxRequiredStateColor);
		waitElementToClick(BizwebFormPage.btnSaveColor);
		exitIframe();
		waitElementToClick(BizwebFormPage.btnSaveForm);
	}

	@Then("^I see form display in frontend$")
	public void i_see_form_display_in_frontend() throws Throwable {
		waitElement(BizwebFormPage.iframeFrontend);
		switchToIframe(BizwebFormPage.iframeFrontend);
		waitElement(BizwebFormPage.titleNameFormFrontend);
		assertText(BizwebFormPage.titleNameFormFrontend, updatevalue);

	}

	@When("^I click submit$")
	public void i_click_Submit() throws Throwable {
		Thread.sleep(3000);
		waitElement(BizwebFormPage.btnSubmitFrontend);
		clickToPoint(BizwebFormPage.btnSubmitFrontend);
	}

	@Then("^I see message error$")
	public void i_see_Message_error() throws Throwable {
		Thread.sleep(1000);
		waitElementWithText(BizwebFormPage.titleErrorFrontend, "Không được để trống");
	}

	@When("^I input data at frontend$")
	public void i_input_Data() throws Throwable {
		waitElement(BizwebFormPage.txtEnterText);
		sendKeys(BizwebFormPage.txtEnterText, enterVanban);
		waitElement(BizwebFormPage.txtEnterNumber);
		sendKeys(BizwebFormPage.txtEnterNumber, enterNumber);
		waitElement(BizwebFormPage.txtEnterEmail);
		sendKeys(BizwebFormPage.txtEnterEmail, enterEmail);
		waitElement(BizwebFormPage.txtEnterPhone);
		sendKeys(BizwebFormPage.txtEnterPhone, enterPhone);
		waitElement(BizwebFormPage.txtMultiLineText);
		sendKeys(BizwebFormPage.txtMultiLineText, "Đây là văn bản nhiều dòng");
		scroll(600, 800);
		waitElement(BizwebFormPage.checkboxManyChoices);
		clickToPoint(BizwebFormPage.checkboxManyChoices);
		waitElement(BizwebFormPage.radioChooseOne);
		clickToPoint(BizwebFormPage.radioChooseOne);
		waitElement(BizwebFormPage.selectDroplistSelection);
		selectDropbox(BizwebFormPage.selectDroplistSelection, 1);
		waitElement(BizwebFormPage.txtDate);
		sendKeys(BizwebFormPage.txtDate, "08/08/2017");
		waitElement(BizwebFormPage.txtTime);
		sendKeys(BizwebFormPage.txtTime, "12:00:00");
		waitElement(BizwebFormPage.txtColor);
		sendKeys(BizwebFormPage.txtColor, "#d23569");
		waitElementToClick(BizwebFormPage.btnSubmitFrontend);
	}

	@Then("^I see message notification$")
	public void i_see_Message_notification() throws Throwable {
		Thread.sleep(3000);
		waitElementWithText(BizwebFormPage.titleMessageSendform, "Cảm ơn bạn đã gửi thông tin thành công!");

	}

	@Then("^I see display times send in list form page$")
	public void display_times_send_in_List_form_Page() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElement(BizwebFormPage.titleTimeSend);
		assertText(BizwebFormPage.titleTimeSend, "1");
	}

	@Then("^I see display information in list send page$")
	public void display_information_in_List_send_Page() throws Throwable {
		waitElement(BizwebFormPage.iconListSend);
		click(BizwebFormPage.iconListSend);
		waitElementWithText(BizwebFormPage.titleContentList, "Trường Văn Bản");
	}

	@When("^I click icon view details$")
	public void i_click_icon_view_details() throws Throwable {
		waitElement(BizwebFormPage.iconViewDetail);
		click(BizwebFormPage.iconViewDetail);
		exitIframe();
	}

	@Then("^I see display view details list send$")
	public void display_view_details_List_send() throws Throwable {
		waitElementWithText(BizwebFormPage.titleContent, "Nội dung chi tiết");
	}

	@Given("^I access list send page$")
	public void i_am_on_List_send_Page() throws Throwable {
		waitElementToClick(BizwebFormPage.btnBack);
	}

	@When("^I click icon delete and delete success$")
	public void i_click_icon_Delete_and_Delete_success() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElementToClick(BizwebFormPage.iconDeleteListSend);
		waitElementToClick(BizwebFormPage.btnAcceptDelete);
		exitIframe();
	}

	@Then("^I see display message$")
	public void i_see_Display_Message() throws Throwable {

	}

	@When("^I create form unsuccess when do not customize content$")
	public void i_Create_form_unSuccess_when_do_not_Customize_content() throws Throwable {
		refresh();
		switchToIframe(BizwebFormPage.iframe);
		waitElement(BizwebFormPage.txtNameForm);
		sendKeys(BizwebFormPage.txtNameForm, value);
		Thread.sleep(2000);
		waitElement(BizwebFormPage.btnContinue);
		click(BizwebFormPage.btnContinue);
		Thread.sleep(2000);
		dragAndDrop(BizwebFormPage.titleTextField, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleEnterNumber, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleEmail, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleEnterPhone, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleNation, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleMultiLineText, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleManyChoices, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleChooseOne, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleDroplistSelection, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleDate, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleTime, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleColor, BizwebFormPage.FormManagement);
		waitElement(BizwebFormPage.btnContinue);
		click(BizwebFormPage.btnContinue);
		waitElement(BizwebFormPage.txtCaptcha);
		clear(BizwebFormPage.txtCaptcha);
		waitElement(BizwebFormPage.txtSendButton);
		clear(BizwebFormPage.txtSendButton);
		waitElement(BizwebFormPage.txtResetButton);
		clear(BizwebFormPage.txtResetButton);
		exitIframe();
		waitElementToClick(BizwebFormPage.btnSaveForm);
	}

	@When("^I create form success when input new customize content$")
	public void i_Create_form_Success_when_input_new_Customize_content() throws Throwable {
		switchToIframe(BizwebFormPage.iframe);
		waitElement(BizwebFormPage.txtCaptcha);
		sendKeys(BizwebFormPage.txtCaptcha, "Nhập mã xác nhận");
		waitElement(BizwebFormPage.txtSendButton);
		sendKeys(BizwebFormPage.txtSendButton, "Nhấn nút Gửi");
		waitElement(BizwebFormPage.txtResetButton);
		sendKeys(BizwebFormPage.txtResetButton, "Nhấn Nhập lại");
		exitIframe();
		waitElementToClick(BizwebFormPage.btnSaveForm);
	}

	@When("^I access update form page input invalid width form in display$")
	public void i_access_update_form_page_input_invalid_Width_form_in_display() throws Throwable {
		waitElementToClick(BizwebFormPage.iconEditFormPage);
		waitElement(BizwebFormPage.txtNameForm);
		clear(BizwebFormPage.txtNameForm);
		sendKeys(BizwebFormPage.txtNameForm, updatevalue);
		waitElement(BizwebFormPage.btnContinue);
		click(BizwebFormPage.btnContinue);
		waitElement(BizwebFormPage.btnContinue);
		click(BizwebFormPage.btnContinue);
		waitElement(BizwebFormPage.txtWidthDesktop);
		clear(BizwebFormPage.txtWidthDesktop);
		sendKeys(BizwebFormPage.txtWidthDesktop, "8");
		exitIframe();
		Thread.sleep(1000);
		waitElementToClick(BizwebFormPage.btnSaveForm);
	}

	@When("^I access update form page when tick display in texbox$")
	public void i_access_update_form_page_when_tick_display_in_texbox() throws Throwable {
		Thread.sleep(2000);
		switchToIframe(BizwebFormPage.iframe);
		waitElement(BizwebFormPage.txtWidthDesktop);
		clear(BizwebFormPage.txtWidthDesktop);
		sendKeys(BizwebFormPage.txtWidthDesktop, "50");
		waitElement(BizwebFormPage.checkboxHiddenTitle);
		clickToPoint(BizwebFormPage.checkboxHiddenTitle);
		exitIframe();
		waitElementToClick(BizwebFormPage.btnSaveForm);
	}

	@When("^Create form unsuccess when blank notification send form success$")
	public void create_form_Unsuccess_when_blank_notification_send_form_success() throws Throwable {
		switchToIframe(BizwebFormPage.iframe);
		waitElement(BizwebFormPage.txtNameForm);
		sendKeys(BizwebFormPage.txtNameForm, value);
		Thread.sleep(2000);
		waitElement(BizwebFormPage.btnContinue);
		click(BizwebFormPage.btnContinue);
		Thread.sleep(2000);
		dragAndDrop(BizwebFormPage.titleTextField, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleEnterNumber, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleEmail, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleEnterPhone, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleNation, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleMultiLineText, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleManyChoices, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleChooseOne, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleDroplistSelection, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleDate, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleTime, BizwebFormPage.FormManagement);
		dragAndDrop(BizwebFormPage.titleColor, BizwebFormPage.FormManagement);
		waitElement(BizwebFormPage.btnContinue);
		click(BizwebFormPage.btnContinue);
		waitElement(BizwebFormPage.btnContinue);
		click(BizwebFormPage.btnContinue);
		Thread.sleep(2000);
		waitElement(BizwebFormPage.iframeMessage);
		switchToIframe(BizwebFormPage.iframeMessage);
		waitElement(BizwebFormPage.txtEnterMassage);
		BizwebFormPage.txtEnterMassage.sendKeys(Keys.CONTROL, "a");
		BizwebFormPage.txtEnterMassage.sendKeys(Keys.DELETE);
		BizwebFormPage.txtEnterMassage.sendKeys(Keys.TAB);
		exitIframe();
		exitIframe();
		Thread.sleep(2000);
		waitElement(BizwebFormPage.btnSaveForm);
		clickToPoint(BizwebFormPage.btnSaveForm);

	}

	@When("^I create form success when input new notification send form success$")
	public void i_Create_form_Success_when_input_new_notification_send_form_success() throws Throwable {
		switchToIframe(BizwebFormPage.iframe);
		waitElement(BizwebFormPage.iframeMessage);
		switchToIframe(BizwebFormPage.iframeMessage);
		waitElement(BizwebFormPage.txtEnterMassage);
		sendKeys(BizwebFormPage.txtEnterMassage, "Thông báo gửi form thành công");
		exitIframe();
		exitIframe();
		waitElementToClick(BizwebFormPage.btnSaveForm);
	}

	@When("^I update form unsuccess when choose have and click save$")
	public void i_update_form_unSuccess_when_choose_have_and_click_Save() throws Throwable {
		waitElementToClick(BizwebFormPage.iconEditFormPage);
		waitElement(BizwebFormPage.txtNameForm);
		clear(BizwebFormPage.txtNameForm);
		sendKeys(BizwebFormPage.txtNameForm, updatevalue);
		waitElement(BizwebFormPage.btnContinue);
		click(BizwebFormPage.btnContinue);
		waitElement(BizwebFormPage.btnContinue);
		click(BizwebFormPage.btnContinue);
		waitElement(BizwebFormPage.btnContinue);
		click(BizwebFormPage.btnContinue);
		waitElement(BizwebFormPage.selectboxFeedback);
		selectDropbox(BizwebFormPage.selectboxFeedback, 0);
		exitIframe();
		waitElement(BizwebFormPage.btnSaveForm);
		click(BizwebFormPage.btnSaveForm);

	}

	@When("^update form success when choose have enter correct and click save$")
	public void update_form_Success_when_choose_have_Enter_correct_and_click_Save() throws Throwable {
		switchToIframe(BizwebFormPage.iframe);
		waitElement(BizwebFormPage.selectboxEmail);
		selectDropbox(BizwebFormPage.selectboxEmail, 1);
		waitElement(BizwebFormPage.titleEmailFeedback);
		sendKeys(BizwebFormPage.titleEmailFeedback, "Tiêu đề Email");
		waitElement(BizwebFormPage.iframeContentEmail);
		switchToIframe(BizwebFormPage.iframeContentEmail);
		waitElement(BizwebFormPage.txtEnterMassage);
		sendKeys(BizwebFormPage.txtEnterMassage, "Nôi dung thông báo Email");
		exitIframe();
		exitIframe();
		waitElementToClick(BizwebFormPage.btnSaveForm);
		Thread.sleep(2000);
	}

	@Then("^I see message notification when update$")
	public void i_see_Message_notification_when_update() throws Throwable {
		Thread.sleep(3000);
		waitElementWithText(BizwebFormPage.titleMessageSendform, "Thông báo gửi form thành công");
	}

	@Then("^Receive email when send form$")
	public void receive_email_when_send_form() throws Throwable {
		openNewTabWindow("https://www.google.com/gmail/about/");
		waitElementToClick(BizwebFormPage.btnSigninGmail);
		loginGmail(enterEmail, "haibib0y1995");
		Thread.sleep(3000);
		waitElementToClick(BizwebFormPage.titleGmail);
		waitElementToClick(BizwebFormPage.btnDeleteMail);
		Thread.sleep(2000);
		closeTabWindow(3);
	}

	@Given("^I access tab window  googledrive$")
	public void i_access_tab_Window_GoogleDrive() throws Throwable {
		openNewTabWindow("https://drive.google.com/");
	}

	@When("^I logout account success$")
	public void i_logout_account_success() throws Throwable {
		switchTabWindow(2);
		waitElementToClick(BizwebFormPage.iconAccount);
		waitElementToClick(BizwebFormPage.btnLogoutAccount);
	}

	@Then("^I see message googledrive is logout$")
	public void i_see_message_GoogleDrive_is_logout() throws Throwable {
		Thread.sleep(5000);
		waitElementWithText(BizwebFormPage.titleNextAccount, "Hi Trần Văn");
		closeTabWindow(2);
	}

	@When("^I remove app success$")
	public void i_remove_app_success() throws Throwable {
		unInstallApp();
	}

	@Then("^I do not see settings page of app$")
	public void i_do_not_see_Settings_page_of_app() throws Throwable {
		Thread.sleep(2000);
		assertText(GlobalPage.contentPage, "Website của bạn chưa cài ứng dụng nào");
	}
}
