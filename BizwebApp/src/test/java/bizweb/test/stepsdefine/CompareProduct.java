package bizweb.test.stepsdefine;

import org.openqa.selenium.support.PageFactory;

import bizweb.test.actions.Base;
import bizweb.test.elements.BizwebFormPage;
import bizweb.test.elements.CompareProductPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CompareProduct extends Base {

	public CompareProduct() {
		PageFactory.initElements(driver, CompareProductPage.class);
		PageFactory.initElements(driver, BizwebFormPage.class);
	}

	@When("^I install app compare product success$")
	public void i_install_app_compare_product_success() throws Throwable {
		installApp("So sánh sản phẩm");
//		Thread.sleep(1000);
	}

	@Then("^I see the app title displayed$")
	public void i_see_the_app_title_displayed() throws Throwable {
		waitElement(CompareProductPage.titleApp);
		assertText(CompareProductPage.titleApp, "So sánh sản phẩm");
	}

	@When("^I click button apply with theme present$")
	public void i_click_button_apply_with_theme_present() throws Throwable {
//		Thread.sleep(1000);
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElement(CompareProductPage.btnApplyTheme);
		clickToPoint(CompareProductPage.btnApplyTheme);
		exitIframe();
	}

	@Then("^I see message apply success$")
	public void i_see_message_apply_success() throws Throwable {
		Thread.sleep(1000);
		waitElementWithText(BizwebFormPage.titleNotification, "Áp dụng vào giao diện mới thành công!");
	}

	@When("^I click button save$")
	public void i_click_button_save() throws Throwable {
		Thread.sleep(1000);
		waitElementToClick(CompareProductPage.btnSave);
		
	}

	@Then("^I see message save success$")
	public void i_see_message_save_success() throws Throwable {
		Thread.sleep(1000);
		waitElementWithText(BizwebFormPage.titleNotification, "Lưu cấu hình thành công!");
	}

	@When("^I click icon view  my website$")
	public void i_click_icon_view_my_website() throws Throwable {
		waitElementToClick(BizwebFormPage.iconViewWebsite);
	}

	@When("^I click product and scroll to product list$")
	public void i_click_product_and_scroll_to_product_list() throws Throwable {
		switchTabWindow(2);
		Thread.sleep(1000);
		waitElementToClick(CompareProductPage.spanProduct);
		scroll(600, 400);
	}

	@Then("^I see have label checkbox$")
	public void i_see_have_checkbox() throws Throwable {
		waitElementWithText(CompareProductPage.labelAddCompareOne, "Thêm vào so sánh");
	}

	@When("^I click checkbox at product want compare$")
	public void i_click_checkbox_at_product_want_compare() throws Throwable {
//		Thread.sleep(1000);
		clickToPoint(CompareProductPage.labelAddCompareOne);
	}

	@Then("^I see message add product compare success$")
	public void i_see_message_add_product_compare_success() throws Throwable {
		waitElementWithText(CompareProductPage.titleAddSuccess, "Thành công");
		Thread.sleep(3000);
	}

	@Then("^I see name tab compare display$")
	public void i_see_name_tab_compare_display() throws Throwable {
		waitElementWithText(CompareProductPage.btnCompareProduct, "So sánh");
	}

	@When("^I click name tab compare$")
	public void i_click_name_tab_compare() throws Throwable {
//		Thread.sleep(1000);
		clickToPoint(CompareProductPage.btnCompareProduct);
	}

	@Then("^I see product display in compare frame$")
	public void i_see_product_display_in_compare_frame() throws Throwable {
		waitElementWithText(CompareProductPage.titleNameProductInFrame, "iPad Wifi 3G 16GB");
	}

	@When("^I click button compare in frame compare$")
	public void i_click_button_compare_in_frame_compare() throws Throwable {
//		Thread.sleep(1000);
		waitElementToClick(CompareProductPage.btnCompareInFrame);
	}

	@Then("^I see compare product display$")
	public void i_see_compare_product_display() throws Throwable {
//		Thread.sleep(1000);
		waitElementWithText(CompareProductPage.titleCompareProductPage, "So sánh sản phẩm");
	}

	@Then("^I see message compare product unsuccess$")
	public void i_see_message_compare_product_unsuccess() throws Throwable {
//		Thread.sleep(1000);
		waitElementWithText(CompareProductPage.titleCompareUnSuccess, "Bạn cần chọn tối thiểu 2 sản phẩm để so sánh!");
	}

	@When("^I click checkbox at product two want compare$")
	public void i_click_checkbox_at_product_two_want_compare() throws Throwable {
//		Thread.sleep(1000);
		clickToPoint(CompareProductPage.labelAddCompareTwo);
	}

	@Then("^I see product two display in compare frame$")
	public void i_see_product_two_display_in_compare_frame() throws Throwable {
		waitElementWithText(CompareProductPage.titleNameProductTwoInFrame, "iPhone 4s 32GB");
	}
	
	@Then("^I see display compare product success$")
	public void i_see_display_compare_product_success() throws Throwable {
//		Thread.sleep(1000);
		waitElementWithText(CompareProductPage.titleCompareSuccess, "Ảnh sản phẩm");
	}
	
	@When("^I click button eliminate at product one$")
	public void i_click_button_eliminate_at_product_one() throws Throwable {
//		Thread.sleep(1000);
		clickToPoint(CompareProductPage.btnEliminateProduct);
	}

	@When("^I click icon Delete$")
	public void i_click_icon_Delete_at_product_one() throws Throwable {
//		Thread.sleep(1000);
		waitElementToClick(CompareProductPage.btnDeleteProduct);
	}
	
	@Given("^I am on config page$")
	public void i_am_on_config_page() throws Throwable {
//		Thread.sleep(1000);
		closeTabWindow(2);
	    switchTabWindow(1);
//	    Thread.sleep(1000);
	    waitElementWithText(CompareProductPage.spanConfig, "Cấu hình");
	}

	@When("^I tick choose type display icon compare$")
	public void i_tick_choose_type_display_icon_compare() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		checkBox(CompareProductPage.checkboxIconCompare);
	}

	@When("^I enter name new invalid at button add compare product$")
	public void i_enter_name_new_invalid_at_button_add_compare_product() throws Throwable {
//		Thread.sleep(1000);
	    waitElement(CompareProductPage.txtButtonAddCompareProduct);
	    clear(CompareProductPage.txtButtonAddCompareProduct);
	    exitIframe();
	}

	@Then("^I see message error display$")
	public void i_see_message_error_display() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElementWithText(CompareProductPage.titleErrorEnterName, "Tên nút không được để trống");
	}

	@When("^I enter name new at button add compare product$")
	public void i_enter_name_new_at_button_add_compare_product() throws Throwable {
//	    Thread.sleep(1000);
	    waitElementToSendKeys(CompareProductPage.txtButtonAddCompareProduct, "Thêm so sánh sản phẩm");
	    exitIframe();
	}
	
	@When("^I click view detail product$")
	public void i_click_view_detail_product() throws Throwable {
	    waitElementToClick(CompareProductPage.btnViewdetail);
	}

	@Then("^I see button add compare product$")
	public void i_see_button_add_compare_product() throws Throwable {
	    Thread.sleep(1000);
	    waitElementWithText(CompareProductPage.btnAddCompare, "Thêm so sánh sản phẩm");
	}

	@When("^I click button add compare product$")
	public void i_click_button_add_compare_product() throws Throwable {
		waitElement(CompareProductPage.btnAddCompare);
	    clickToPoint(CompareProductPage.btnAddCompare);
	}
	
	@When("^I click icon compare$")
	public void i_click_icon_compare() throws Throwable {
//		Thread.sleep(1000);
		clickToPoint(CompareProductPage.btnCompareProduct);
	}
	
	@Then("^I click checkbox at product three want compare$")
	public void i_click_checkbox_at_product_three_want_compare() throws Throwable {
//		Thread.sleep(1000);
		clickToPoint(CompareProductPage.labelAddCompareThree);
	}

	@Then("^I click checkbox at product four want compare$")
	public void i_click_checkbox_at_product_four_want_compare() throws Throwable {
//		Thread.sleep(1000);
		clickToPoint(CompareProductPage.labelAddCompareFour);
	}
	
	@Then("^I see product three display in compare frame$")
	public void i_see_product_three_display_in_compare_frame() throws Throwable {
		waitElementWithText(CompareProductPage.titleNameProductThreeInFrame, "Nokia X6 8GB");
	}

	@Then("^I see product four display in compare frame$")
	public void i_see_product_four_display_in_compare_frame() throws Throwable {
		waitElementWithText(CompareProductPage.titleNameProductFourInFrame, "iPad 2 Wifi 64GB");
	}
	
	@When("^I tick choose type display tab compare$")
	public void i_tick_choose_type_display_tab_compare() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		checkBox(CompareProductPage.checkboxTabCompare);
	}

	@When("^I enter name new valid at button add compare product$")
	public void i_enter_name_new_valid_at_button_add_compare_product() throws Throwable {
//		Thread.sleep(1000);
	    waitElement(CompareProductPage.txtButtonAddCompareProduct);
	    clear(CompareProductPage.txtButtonAddCompareProduct);
	    sendKeys(CompareProductPage.txtButtonAddCompareProduct, "Thêm so sánh");
	    exitIframe();
	}
	
	@Then("^I click checkbox at product five want compare$")
	public void i_click_checkbox_at_product_five_want_compare() throws Throwable {
//		Thread.sleep(1000);
		clickToPoint(CompareProductPage.labelAddCompareFive);
	}

	@Then("^I see message error with add compare product$")
	public void i_see_message_error_with_add_compare_product() throws Throwable {
	    
	}
	
}
