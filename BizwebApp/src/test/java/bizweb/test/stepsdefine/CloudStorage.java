package bizweb.test.stepsdefine;

import org.openqa.selenium.support.PageFactory;

import bizweb.test.actions.Base;
import bizweb.test.actions.Const;
import bizweb.test.elements.BizwebFormPage;
import bizweb.test.elements.CloudStoragePage;
import bizweb.test.elements.GlobalPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CloudStorage extends Base {
	public String path = System.getProperty("user.dir") + "\\src\\test\\resources\\FileUpload\\";

	public CloudStorage() {
		PageFactory.initElements(driver, CloudStoragePage.class);
		PageFactory.initElements(driver, BizwebFormPage.class);
		PageFactory.initElements(driver, GlobalPage.class);
	}

	@When("^I install app data success$")
	public void i_install_app_data_success() throws Throwable {
		refresh();
		installApp("Quản lý dữ liệu");
	}

	@Then("^I see form connect in app page$")
	public void i_see_form_connect_in_app_page() throws Throwable {
		Thread.sleep(2000);
		assertText(GlobalPage.titlePage, "Quản lý dữ liệu");
	}

	@When("^I connect account googledrive success$")
	public void connect_account_GoogleDrive_success() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElementToClick(CloudStoragePage.iconConnectAccount);
		switchTabWindow(2);
		waitElementToClick(BizwebFormPage.btnUserAnotherAccount);
		waitElement(GlobalPage.emailGoogle);
		sendKeys(GlobalPage.emailGoogle, Const.EMAIL);
		waitElementToClick(BizwebFormPage.btnNext1);
		waitElement(GlobalPage.passGoogle);
		sendKeys(GlobalPage.passGoogle, Const.PASSGMAIL);
		waitElementToClick(BizwebFormPage.btnNext2);
		waitElementToClick(BizwebFormPage.btnAllow);
		exitIframe();
		
	}

	@Then("^I see account is connected$")
	public void i_see_account_GoogleDrive_is_connected() throws Throwable {
		switchTabWindow(1);
		Thread.sleep(2000);
		switchToIframe(BizwebFormPage.iframe);
		Thread.sleep(2000);
		assertText(CloudStoragePage.titleAccount, "Danh sách tài khoản");
		//screen("titleAccount");
	}

	@Given("^I access login account$")
	public void i_access_Connect_acount_GoogleDrive_Page() throws Throwable {
		waitElementToClick(CloudStoragePage.btnLogin);
		exitIframe();
	}

	@When("^I click button new folder$")
	public void i_click_button_Folder_new() throws Throwable {
		waitElement(CloudStoragePage.btnCreateFolder);
		click(CloudStoragePage.btnCreateFolder);
	}

	@When("^I enter valid name$")
	public void i_enter_valid_name() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElement(CloudStoragePage.txtNewFolder);
		sendKeys(CloudStoragePage.txtNewFolder, "Thư mục một");
	}

	@When("^I click button create$")
	public void i_click_button_Create() throws Throwable {
		waitElementToClick(CloudStoragePage.btnCreateNewFolder);
		exitIframe();
	}

	@Then("^I see message sucess display$")
	public void i_see_message_sucess_display() throws Throwable {
		Thread.sleep(1000);
		waitElement(BizwebFormPage.titleNotification);
		assertText(BizwebFormPage.titleNotification, "Thao tác thành công!");
	}

	@Then("^I see folder have display$")
	public void i_see_folder_have_display() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElement(CloudStoragePage.titleNameFolder);
		assertText(CloudStoragePage.titleNameFolder, "Thư mục một");
		
	}
	
	@When("^I choose file invalid$")
	public void i_choose_file_invalid() throws Throwable {
		sendKeys(CloudStoragePage.inputUpload, ""+ path +"Driver.zip");
		Thread.sleep(1000);
	}

	@Then("^I see message uploadFile unsuccess$")
	public void i_see_message_uploadFile_unsuccess() throws Throwable {
		waitElementWithText(CloudStoragePage.titleErrorUploadFile, "Bạn không thể upload file > 25MB!");
	}

	@When("^I choose file valid$")
	public void i_choose_file_valid() throws Throwable {
		sendKeys(CloudStoragePage.inputUpload, ""+ path +"Toolquanlyloi.docx");
		Thread.sleep(2000);
	}

	@Then("^I see message uploadFile success$")
	public void i_see_message_uploadFile_success() throws Throwable {
		waitElementWithText(CloudStoragePage.titleNameFile, "Toolquanlyloi.docx");
	}

	@When("^I click view before file$")
	public void i_click_view_before_uploadFile() throws Throwable {
		waitElementToClick(CloudStoragePage.iconViewFile);
		Thread.sleep(2000);
		exitIframe();
		switchTabWindow(2);
	}

	@Then("^I see file at googledrive$")
	public void i_see_file_at_GoogleDrive() throws Throwable {
		waitElement(CloudStoragePage.titleNameFileAtGoogleDrivePage);
		assertText(CloudStoragePage.titleNameFileAtGoogleDrivePage, "Toolquanlyloi.docx");
		closeTabWindow(2);
	}

	@When("^I click get link file$")
	public void i_click_get_link_file() throws Throwable {
		switchTabWindow(1);
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElementToClick(CloudStoragePage.iconLinkFile);
		waitElementToClick(CloudStoragePage.btnCopy);
		waitElementToClick(BizwebFormPage.btnCloseEmbedcode);
		Thread.sleep(2000);
	}

	@When("^I enter name file or folder invalid$")
	public void i_enter_invalid_name_file_folder() throws Throwable {
	    waitElementToSendKeys(CloudStoragePage.txtSearch, "search file or folder");
	    Thread.sleep(2000);
	}

	@Then("^I dont see file or folder with name file or folder have enter$")
	public void i_dont_see_file_or_folder_with_name_file_folder_enter() throws Throwable {
	    
	}

	@When("^I enter name file or folder valid$")
	public void i_enter_valid_name_file_folder() throws Throwable {
		clear(CloudStoragePage.txtSearch);
		waitElementToSendKeys(CloudStoragePage.txtSearch, "Toolquanly");
	}

	@Then("^I see file or folder with name file, folder enter$")
	public void i_see_file_or_folder_with_name_file_folder_enter() throws Throwable {
		waitElement(CloudStoragePage.titleNameFileAtFolder);
		assertText(CloudStoragePage.titleNameFileAtFolder,"Toolquanlyloi.docx");
	}
	
	@When("^I click icon download$")
	public void i_click_icon_Download() throws Throwable {
		waitElementToClick(CloudStoragePage.iconDownload);
		Thread.sleep(2000);
	}

	@Then("^I see file have download$")
	public void i_see_File_have_download() throws Throwable {

	}
	
	@When("^I click icon move file$")
	public void i_click_icon_move_File() throws Throwable {
		switchTabWindow(1);
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElementToClick(CloudStoragePage.iconMove);
	}
	
	@When("^I choose folder contains file move$")
	public void i_choose_folder() throws Throwable {
		Thread.sleep(1000);
		waitElementToClick(CloudStoragePage.titleFolderContainsFile);
		Thread.sleep(1000);
		waitElementToClick(CloudStoragePage.btnAccept);
		exitIframe();
	}

	@Given("^I access to folder$")
	public void i_access_to_Folder() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		Thread.sleep(2000);
		clear(CloudStoragePage.txtSearch);
		waitElement(CloudStoragePage.titleNameFolder);
		doubleClick(CloudStoragePage.titleNameFolder);
	}

	@When("^I click file$")
	public void i_click_File() throws Throwable {
		Thread.sleep(2000);
		waitElement(CloudStoragePage.titleNameFileAtFolder);
		click(CloudStoragePage.titleNameFileAtFolder);
	}

	@When("^I click icon copyfile$")
	public void i_click_icon_CopyFile() throws Throwable {
		waitElementToClick(CloudStoragePage.iconCopyFile);
	}

	@When("^I choose folder contains file copy$")
	public void i_choose_folder_file() throws Throwable {
		Thread.sleep(1000);
		waitElementToClick(CloudStoragePage.titleAccountContainsFile);
		waitElementToClick(CloudStoragePage.btnAccept);
		exitIframe();
	}

	@When("^I click icon delete file$")
	public void i_click_icon_Delete_file() throws Throwable {
		Thread.sleep(2000);
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElementToClick(CloudStoragePage.BackFolderOrFile);
		Thread.sleep(1000);
		waitElementToClick(CloudStoragePage.titleNameFile);
		waitElementToClick(CloudStoragePage.iconDeleteFile);
	}

	@When("^I click delete$")
	public void i_click_Delete() throws Throwable {
		waitElementToClick(CloudStoragePage.btnAccept);
		exitIframe();
	}

	@When("^I click view before$")
	public void i_click_view_before() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		Thread.sleep(2000);
		waitElementToClick(CloudStoragePage.iconViewFolder);
		Thread.sleep(2000);
		exitIframe();
		switchTabWindow(2);
	}

	@Then("^I see folder at googledrive$")
	public void i_see_folder_at_GoogleDrive() throws Throwable {
		waitElement(CloudStoragePage.titleNameFolderAtGoogleDrivePage);
		assertText(CloudStoragePage.titleNameFolderAtGoogleDrivePage, "Thư mục một");
		closeTabWindow(2);
		switchTabWindow(1);
	}

	@When("^I click get link$")
	public void i_click_get_link() throws Throwable {
		
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElementToClick(CloudStoragePage.iconLinkFolder);
		waitElementToClick(CloudStoragePage.btnCopy);
		waitElementToClick(BizwebFormPage.btnCloseEmbedcode);
		Thread.sleep(2000);

	}

	@When("^I copy link in new tab window$")
	public void i_copy_link_in_new_tab_window() throws Throwable {

	}

	@When("^I click folder$")
	public void i_click_Folder() throws Throwable {
		Thread.sleep(2000);
		waitElementToClick(CloudStoragePage.titleNameFolder);
	}

	@When("^I click icon delete$")
	public void i_click_icon_Delete() throws Throwable {
		waitElementToClick(CloudStoragePage.iconDelete);
	}

	@When("^I perform logout success$")
	public void i_perform_logout_success() throws Throwable {
		Thread.sleep(2000);
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElementToClick(CloudStoragePage.spanAccount);
		waitElementToClick(CloudStoragePage.btnDeleteAccount);
		waitElementToClick(CloudStoragePage.btnAccept);
		//exitIframe();
	}

	@Then("^I see message delete success$")
	public void i_see_message_delete_success() throws Throwable {
		
	}

	@When("^Connect account dropbox success$")
	public void connect_account_Dropbox_success() throws Throwable {
		refresh();
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElementToClick(CloudStoragePage.iconDropbox);
		switchTabWindow(2);
		waitElement(CloudStoragePage.txtEmailDropbox);
		sendKeys(CloudStoragePage.txtEmailDropbox, "haitranvan188@gmail.com");
		waitElement(CloudStoragePage.txtPassDropbox);
		sendKeys(CloudStoragePage.txtPassDropbox, "h@ibib0y");
		waitElement(CloudStoragePage.btnSigninDropbox);
		clickToPoint(CloudStoragePage.btnSigninDropbox);
//		waitElementToClick(AppDulieuPage.btnAllow);
		exitIframe();
		Thread.sleep(2000);
	}
	
	@When("^I click button folder new two$")
	public void i_click_button_Folder_new_two() throws Throwable {
	    waitElementToClick(CloudStoragePage.btnCreateFolder);
	}
	
	@Then("^I see folder have display account Dropbox$")
	public void i_see_folder_have_display_account_Dropbox() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElement(CloudStoragePage.titleNameFolder);
		assertText(CloudStoragePage.titleNameFolder, "Thư mục một");
	}
	
	@When("^I enter already exists name$")
	public void i_enter_already_exists_name() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElement(CloudStoragePage.txtNewFolder);
		sendKeys(CloudStoragePage.txtNewFolder, "Thư mục một");
	}

	@Then("^I see message error already exists name display$")
	public void i_see_message_error_already_exists_name_display() throws Throwable {
		Thread.sleep(1000);
		waitElement(BizwebFormPage.titleNotification);
		assertText(BizwebFormPage.titleNotification, "Thư mục đã tồn tại. Không thể thực hiện thao tác này!");
	}

	@When("^I enter valid name two$")
	public void i_enter_valid_name_two() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElement(CloudStoragePage.txtNewFolder);
		sendKeys(CloudStoragePage.txtNewFolder, "Thư mục thứ hai");
	}

	@Then("^I see folder have display two$")
	public void i_see_folder_have_display_two() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		clear(CloudStoragePage.txtSearch);
		Thread.sleep(2000);
		waitElement(CloudStoragePage.titleNameFolderTwo);
		assertText(CloudStoragePage.titleNameFolderTwo, "Thư mục thứ hai");
		//exitIframe();
	}
	
	@Given("^I access to folder at dropbox$")
	public void i_access_to_Folder_at_Dropbox() throws Throwable {
		clear(CloudStoragePage.txtSearch);
		Thread.sleep(2000);
		waitElement(CloudStoragePage.titleNameFolder);
		doubleClick(CloudStoragePage.titleNameFolder);
	}
	
	@When("^I choose folder file two$")
	public void i_choose_folder_file_two() throws Throwable {
		waitElementToClick(CloudStoragePage.titleFolderTwoContainsFile);
		waitElementToClick(CloudStoragePage.btnAccept);
		exitIframe();
	}
	
	@Then("^I back to folder or file$")
	public void i_back_to_Folder_or_file() throws Throwable {
		Thread.sleep(2000);
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe);
		waitElementToClick(CloudStoragePage.BackFolderOrFile);
	}
	
	@Then("^I see file at folder two$")
	public void i_see_file_at_Folder_two() throws Throwable {
		waitElement(CloudStoragePage.titleNameFolderTwo);
		doubleClick(CloudStoragePage.titleNameFolderTwo);
		Thread.sleep(2000);
		waitElement(CloudStoragePage.titleNameFileAtFolder);
		assertText(CloudStoragePage.titleNameFileAtFolder,"Toolquanlyloi.docx");
		
	}
	
	@When("^I click icon delete file at folder two$")
	public void i_click_icon_Delete_file_at_folder_two() throws Throwable {
		Thread.sleep(2000);
		waitElementToClick(CloudStoragePage.titleNameFileAtFolder);
		waitElementToClick(CloudStoragePage.iconDeleteFile);
	}
	
	@When("^I click folder at dropbox$")
	public void i_click_Folder_at_Dropbox() throws Throwable {
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe); 
		Thread.sleep(2000);
		waitElementToClick(CloudStoragePage.titleNameFolder);
	}
	
	@When("^I click icon delete folder at folder two$")
	public void i_click_icon_Delete_folder_at_folder_two() throws Throwable {
	    Thread.sleep(2000);
	    waitElementToClick(CloudStoragePage.titleNameFolderTwo);
//	    exitIframe();
	}
	
	@When("^Connect account onedrive success$")
	public void connect_account_OneDrive_success() throws Throwable {
		refresh();
		waitElement(BizwebFormPage.iframe);
		switchToIframe(BizwebFormPage.iframe); 
		waitElementToClick(CloudStoragePage.iconOneDrive);
		switchTabWindow(2);
		waitElement(CloudStoragePage.txtEmailOneDrive);
		sendKeys(CloudStoragePage.txtEmailOneDrive, "hainguyen1232123@outlook.com");
		waitElement(CloudStoragePage.btnNext);
		click(CloudStoragePage.btnNext);
		waitElement(CloudStoragePage.txtPassOneDrive);
		sendKeys(CloudStoragePage.txtPassOneDrive, "A1232123");
		waitElement(CloudStoragePage.btnSigninOneDrive);
		click(CloudStoragePage.btnSigninOneDrive);
//		exitIframe();
		Thread.sleep(2000);
	}

	@Then("^I see file at onedrive$")
	public void i_see_file_at_OneDrive() throws Throwable {
		waitElement(CloudStoragePage.iframeOneDrive);
		switchToIframe(CloudStoragePage.iframeOneDrive);
		waitElement(CloudStoragePage.titleNameFileAtOneDrive);
		assertText(CloudStoragePage.titleNameFileAtOneDrive, "Toolquanlyloi");
		closeTabWindow(2);
	}
	
	@When("^I perform download$")
	public void i_perform_Download() throws Throwable {
		waitElementToClick(CloudStoragePage.btnCopy);
		waitElementToClick(BizwebFormPage.btnCloseEmbedcode);
		Thread.sleep(2000);
	}
	
	@Then("^I see folder at onedrive$")
	public void i_see_folder_at_OneDrive() throws Throwable {
		Thread.sleep(2000);
		waitElement(CloudStoragePage.titleNameFolderOneDrive);
		assertText(CloudStoragePage.titleNameFolderOneDrive, "Thư mục một");
		closeTabWindow(2);
		switchTabWindow(1);
	}
	
	@Then("^I dont see have account connect$")
	public void i_see_dont_have_account_connect() throws Throwable {
		Thread.sleep(2000);
	    waitElementWithText(CloudStoragePage.titleDontAccountConnect, "Hiển thị kết quả từ 0 - 0 trên tổng 0");
	    exitIframe();
	}
}
