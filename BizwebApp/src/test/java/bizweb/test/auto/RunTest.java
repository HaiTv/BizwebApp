package bizweb.test.auto;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.chrome.ChromeDriver;

import bizweb.test.actions.Base;
import bizweb.test.actions.Const;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
				features = {"src/test/resources/Features"},
				format = {"html:target/site/cucumber-pretty", "json:target/cucumber.json"},
				glue="bizweb.test.stepsdefine"
				)

public class RunTest extends Base{
	@BeforeClass
	public static void startUp(){
		System.out.println("Opening Chrome Browser ...");
		System.setProperty("webdriver.chrome.driver", Const.PATH);
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}
	
	@AfterClass
	public static void tearDown() throws InterruptedException {
		Thread.sleep(3000);
		driver.close();
	}
}
