$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("AppLogin.feature");
formatter.feature({
  "line": 1,
  "name": "Login success",
  "description": "I want to login success",
  "id": "login-success",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "Login success",
  "description": "",
  "id": "login-success;login-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I access admin site",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I login success",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I can access apps menu",
  "keyword": "Then "
});
formatter.match({
  "location": "Global.i_access_admin_site()"
});
formatter.result({
  "duration": 3894299239,
  "status": "passed"
});
formatter.match({
  "location": "Global.i_login_success()"
});
formatter.result({
  "duration": 6808991604,
  "status": "passed"
});
formatter.match({
  "location": "Global.i_can_access_apps_menu()"
});
formatter.result({
  "duration": 1847142649,
  "status": "passed"
});
formatter.uri("BizwebForm.feature");
formatter.feature({
  "line": 1,
  "name": "BizwebForm",
  "description": "I would like to collect customer information by creating a survey form on my website.",
  "id": "bizwebform",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "Install app success",
  "description": "",
  "id": "bizwebform;install-app-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I install app success",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "I see form display in app page",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_install_app_success()"
});
formatter.result({
  "duration": 15283305186,
  "status": "passed"
});
formatter.match({
  "location": "BizwebForm.i_see_form_display_in_app_page()"
});
formatter.result({
  "duration": 85936287,
  "status": "passed"
});
formatter.scenario({
  "line": 8,
  "name": "Create form unsuccess when do not input name form",
  "description": "",
  "id": "bizwebform;create-form-unsuccess-when-do-not-input-name-form",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 9,
  "name": "I am on list form page",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "I access create form page",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I perform create form unsuccess",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I see message error with create form",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_am_on_List_form_Page()"
});
formatter.result({
  "duration": 3006570054,
  "status": "passed"
});
formatter.match({
  "location": "BizwebForm.i_access_Create_form_page()"
});
formatter.result({
  "duration": 178739420,
  "status": "passed"
});
formatter.match({
  "location": "BizwebForm.create_form_Unsuccess()"
});
formatter.result({
  "duration": 1201397831,
  "status": "passed"
});
formatter.match({
  "location": "BizwebForm.i_see_message_error_with_create_form()"
});
formatter.result({
  "duration": 161115785,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Create form success when input name form",
  "description": "",
  "id": "bizwebform;create-form-success-when-input-name-form",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 15,
  "name": "I perform input data",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "I see message create success",
  "keyword": "Then "
});
formatter.step({
  "line": 17,
  "name": "I see form display in from list when create",
  "keyword": "And "
});
formatter.match({
  "location": "BizwebForm.input_Data()"
});
formatter.result({
  "duration": 2674759027,
  "status": "passed"
});
formatter.match({
  "location": "BizwebForm.i_see_message_success_when_enter_name()"
});
formatter.result({
  "duration": 676483757,
  "status": "passed"
});
formatter.match({
  "location": "BizwebForm.form_Display_in_from_list_when_create()"
});
formatter.result({
  "duration": 512375676,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "Update form success when input new name form",
  "description": "",
  "id": "bizwebform;update-form-success-when-input-new-name-form",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 20,
  "name": "I update form success when enter new name",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "I see message update success",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "I see form display in from list when update",
  "keyword": "And "
});
formatter.match({
  "location": "BizwebForm.update_form_Success_when_enter_new_name()"
});
formatter.result({
  "duration": 3208254841,
  "status": "passed"
});
formatter.match({
  "location": "BizwebForm.i_see_message_success()"
});
formatter.result({
  "duration": 655331546,
  "status": "passed"
});
formatter.match({
  "location": "BizwebForm.form_Display_in_from_list_when_update()"
});
formatter.result({
  "duration": 1515566832,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Copy embedded code success",
  "description": "",
  "id": "bizwebform;copy-embedded-code-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 25,
  "name": "I click icon embedded code",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "I copy embedded code and paste in templace",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I click display frontend page",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "I do not see form display in frontend",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_Click_icon_Embedded_code()"
});
formatter.result({
  "duration": 2679128800,
  "status": "passed"
});
formatter.match({
  "location": "BizwebForm.i_copy_Embedded_code_and_paste_in_Templace()"
});
formatter.result({
  "duration": 11186484241,
  "status": "passed"
});
formatter.match({
  "location": "BizwebForm.display_Frontend_Page()"
});
formatter.result({
  "duration": 8334280730,
  "status": "passed"
});
formatter.match({
  "location": "BizwebForm.i_do_not_see_form_display_in_frontend()"
});
formatter.result({
  "duration": 3000590163,
  "status": "passed"
});
formatter.scenario({
  "line": 30,
  "name": "Display list form success",
  "description": "",
  "id": "bizwebform;display-list-form-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 31,
  "name": "I access list form page",
  "keyword": "When "
});
formatter.match({
  "location": "BizwebForm.i_access_List_form_page()"
});
formatter.result({
  "duration": 441680134,
  "error_message": "org.openqa.selenium.InvalidElementStateException: invalid element state: Element must be user-editable in order to clear it.\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 24 milliseconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11392_212}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: b74804b9997f18cfcb5f47c5331ecac8\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:268)\r\n\tat org.openqa.selenium.remote.RemoteWebElement.clear(RemoteWebElement.java:113)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\r\n\tat java.lang.reflect.Method.invoke(Unknown Source)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:50)\r\n\tat com.sun.proxy.$Proxy16.clear(Unknown Source)\r\n\tat bizweb.test.actions.WebDriverAction.clear(WebDriverAction.java:71)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_access_List_form_page(BizwebForm.java:165)\r\n\tat ✽.When I access list form page(BizwebForm.feature:31)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 33,
  "name": "Display list send form empty",
  "description": "",
  "id": "bizwebform;display-list-send-form-empty",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 34,
  "name": "I access list send form",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "I see list send form empty",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_access_list_send_form()"
});
formatter.result({
  "duration": 24203526,
  "error_message": "org.openqa.selenium.NoSuchElementException: no such element: Unable to locate element: {\"method\":\"xpath\",\"selector\":\"//iframe[contains(@class,\u0027app-iframe\u0027)]\"}\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 22 milliseconds\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11392_212}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: b74804b9997f18cfcb5f47c5331ecac8\n*** Element info: {Using\u003dxpath, value\u003d//iframe[contains(@class,\u0027app-iframe\u0027)]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.getWrappedElement(Unknown Source)\r\n\tat org.openqa.selenium.remote.internal.WebElementToJsonConverter.apply(WebElementToJsonConverter.java:48)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver$RemoteTargetLocator.frame(RemoteWebDriver.java:879)\r\n\tat bizweb.test.actions.WebDriverAction.switchToIframe(WebDriverAction.java:87)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_access_list_send_form(BizwebForm.java:288)\r\n\tat ✽.When I access list send form(BizwebForm.feature:34)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_see_List_send_form_empty()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 37,
  "name": "Delete form success",
  "description": "",
  "id": "bizwebform;delete-form-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 38,
  "name": "I perform back list form",
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "I perform a delete form",
  "keyword": "And "
});
formatter.step({
  "line": 40,
  "name": "I see display message delete form",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.back_List_form()"
});
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          formatter.result({
  "duration": 4013327420,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver$RemoteTargetLocator.defaultContent(RemoteWebDriver.java:897)\r\n\tat bizweb.test.actions.WebDriverAction.exitIframe(WebDriverAction.java:91)\r\n\tat bizweb.test.stepsdefine.BizwebForm.back_List_form(BizwebForm.java:300)\r\n\tat ✽.When I perform back list form(BizwebForm.feature:38)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_Perform_a_delete_form()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_see_Display_Message_delete_form()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 42,
  "name": "Create form success when pull all information feild",
  "description": "",
  "id": "bizwebform;create-form-success-when-pull-all-information-feild",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 43,
  "name": "I access create form page",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "I perform create form success with pull all information feild",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "I see message create success",
  "keyword": "Then "
});
formatter.step({
  "line": 46,
  "name": "I see form display in from list when create",
  "keyword": "And "
});
formatter.match({
  "location": "BizwebForm.i_access_Create_form_page()"
});
formatter.result({
  "duration": 4009585017,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//a[contains(.,\u0027Tạo form\u0027)]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:563)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:556)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElementToClick(WebDriverAction.java:191)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_access_Create_form_page(BizwebForm.java:47)\r\n\tat ✽.When I access create form page(BizwebForm.feature:43)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.create_form_Success_with_pull_all_information_feild()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_see_message_success_when_enter_name()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.form_Display_in_from_list_when_create()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 48,
  "name": "Update form success when choose required state",
  "description": "",
  "id": "bizwebform;update-form-success-when-choose-required-state",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 49,
  "name": "I update form success when choose required state",
  "keyword": "When "
});
formatter.step({
  "line": 50,
  "name": "I see message update success",
  "keyword": "Then "
});
formatter.step({
  "line": 51,
  "name": "I see form display in from list when update",
  "keyword": "And "
});
formatter.match({
  "location": "BizwebForm.i_Update_form_Success()"
});
formatter.result({
  "duration": 4013907747,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//a[@title\u003d\u0027Chi tiết form\u0027]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:563)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:556)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElementToClick(WebDriverAction.java:191)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_Update_form_Success(BizwebForm.java:347)\r\n\tat ✽.When I update form success when choose required state(BizwebForm.feature:49)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_see_message_success()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.form_Display_in_from_list_when_update()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 53,
  "name": "Display in frontend success",
  "description": "",
  "id": "bizwebform;display-in-frontend-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 54,
  "name": "I click icon embedded code",
  "keyword": "When "
});
formatter.step({
  "line": 55,
  "name": "I copy embedded code and paste in templace",
  "keyword": "And "
});
formatter.step({
  "line": 56,
  "name": "I click display frontend page",
  "keyword": "And "
});
formatter.step({
  "line": 57,
  "name": "I see form display in frontend",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_Click_icon_Embedded_code()"
});
formatter.result({
  "duration": 4011293069,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//a[@title\u003d\u0027Mã nhúng\u0027]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:563)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:556)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElementToClick(WebDriverAction.java:191)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_Click_icon_Embedded_code(BizwebForm.java:108)\r\n\tat ✽.When I click icon embedded code(BizwebForm.feature:54)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_copy_Embedded_code_and_paste_in_Templace()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.display_Frontend_Page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_see_form_display_in_frontend()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 59,
  "name": "I check required state success",
  "description": "",
  "id": "bizwebform;i-check-required-state-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 60,
  "name": "I click submit",
  "keyword": "When "
});
formatter.step({
  "line": 61,
  "name": "I see message error",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_click_Submit()"
});
formatter.result({
  "duration": 7012313452,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//*[@id\u003d\u0027btnSubmit\u0027]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElement(WebDriverAction.java:187)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_click_Submit(BizwebForm.java:418)\r\n\tat ✽.When I click submit(BizwebForm.feature:60)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_see_Message_error()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 63,
  "name": "I send information frontend",
  "description": "",
  "id": "bizwebform;i-send-information-frontend",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 64,
  "name": "I input data at frontend",
  "keyword": "When "
});
formatter.step({
  "line": 65,
  "name": "I see message notification",
  "keyword": "Then "
});
formatter.step({
  "line": 66,
  "name": "I access list form page",
  "keyword": "And "
});
formatter.step({
  "line": 67,
  "name": "I see display times send in list form page",
  "keyword": "And "
});
formatter.step({
  "line": 68,
  "name": "I see display information in list send page",
  "keyword": "And "
});
formatter.match({
  "location": "BizwebForm.i_input_Data()"
});
formatter.result({
  "duration": 4011250731,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//*[@id\u003d\u0027Data_0__Result\u0027]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElement(WebDriverAction.java:187)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_input_Data(BizwebForm.java:430)\r\n\tat ✽.When I input data at frontend(BizwebForm.feature:64)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_see_Message_notification()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_access_List_form_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.display_times_send_in_List_form_Page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.display_information_in_List_send_Page()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 70,
  "name": "View list send at form success",
  "description": "",
  "id": "bizwebform;view-list-send-at-form-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 71,
  "name": "I click icon view details",
  "keyword": "When "
});
formatter.step({
  "line": 72,
  "name": "I see display view details list send",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_click_icon_view_details()"
});
formatter.result({
  "duration": 4013325282,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//tr[1]/td[7]/a[1]/i}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElement(WebDriverAction.java:187)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_click_icon_view_details(BizwebForm.java:480)\r\n\tat ✽.When I click icon view details(BizwebForm.feature:71)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.display_view_details_List_send()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 74,
  "name": "Delete list send success",
  "description": "",
  "id": "bizwebform;delete-list-send-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 75,
  "name": "I access list send page",
  "keyword": "Given "
});
formatter.step({
  "line": 76,
  "name": "I click icon delete and delete success",
  "keyword": "When "
});
formatter.step({
  "line": 77,
  "name": "I see display message",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_am_on_List_send_Page()"
});
formatter.result({
  "duration": 4019349649,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.02 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//*[@id\u003d\u0027content\u0027]/div/div[2]/div/a}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:563)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:556)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElementToClick(WebDriverAction.java:191)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_am_on_List_send_Page(BizwebForm.java:492)\r\n\tat ✽.Given I access list send page(BizwebForm.feature:75)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_click_icon_Delete_and_Delete_success()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_see_Display_Message()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 79,
  "name": "Delete form success",
  "description": "",
  "id": "bizwebform;delete-form-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 80,
  "name": "I perform back list form",
  "keyword": "When "
});
formatter.step({
  "line": 81,
  "name": "I perform a delete form",
  "keyword": "And "
});
formatter.step({
  "line": 82,
  "name": "I see display message delete form",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.back_List_form()"
});
formatter.result({
  "duration": 4012060281,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver$RemoteTargetLocator.defaultContent(RemoteWebDriver.java:897)\r\n\tat bizweb.test.actions.WebDriverAction.exitIframe(WebDriverAction.java:91)\r\n\tat bizweb.test.stepsdefine.BizwebForm.back_List_form(BizwebForm.java:300)\r\n\tat ✽.When I perform back list form(BizwebForm.feature:80)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_Perform_a_delete_form()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_see_Display_Message_delete_form()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 84,
  "name": "Create form unsuccess when do not customize content",
  "description": "",
  "id": "bizwebform;create-form-unsuccess-when-do-not-customize-content",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 85,
  "name": "I access create form page",
  "keyword": "When "
});
formatter.step({
  "line": 86,
  "name": "I create form unsuccess when do not customize content",
  "keyword": "And "
});
formatter.step({
  "line": 87,
  "name": "I see message error with create form",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_access_Create_form_page()"
});
formatter.result({
  "duration": 4014661274,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//a[contains(.,\u0027Tạo form\u0027)]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:563)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:556)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElementToClick(WebDriverAction.java:191)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_access_Create_form_page(BizwebForm.java:47)\r\n\tat ✽.When I access create form page(BizwebForm.feature:85)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_Create_form_unSuccess_when_do_not_Customize_content()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_see_message_error_with_create_form()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 89,
  "name": "Create form success when input new customize content",
  "description": "",
  "id": "bizwebform;create-form-success-when-input-new-customize-content",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 90,
  "name": "I create form success when input new customize content",
  "keyword": "When "
});
formatter.step({
  "line": 91,
  "name": "I see message create success",
  "keyword": "Then "
});
formatter.step({
  "line": 92,
  "name": "I see form display in from list when create",
  "keyword": "And "
});
formatter.match({
  "location": "BizwebForm.i_Create_form_Success_when_input_new_Customize_content()"
});
formatter.result({
  "duration": 4008139545,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//iframe[contains(@class,\u0027app-iframe\u0027)]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.getWrappedElement(Unknown Source)\r\n\tat org.openqa.selenium.remote.internal.WebElementToJsonConverter.apply(WebElementToJsonConverter.java:48)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver$RemoteTargetLocator.frame(RemoteWebDriver.java:879)\r\n\tat bizweb.test.actions.WebDriverAction.switchToIframe(WebDriverAction.java:87)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_Create_form_Success_when_input_new_Customize_content(BizwebForm.java:545)\r\n\tat ✽.When I create form success when input new customize content(BizwebForm.feature:90)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_see_message_success_when_enter_name()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.form_Display_in_from_list_when_create()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 94,
  "name": "Update form unsuccess when input width form in display \u003e 720px value 8",
  "description": "",
  "id": "bizwebform;update-form-unsuccess-when-input-width-form-in-display-\u003e-720px-value-8",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 95,
  "name": "I access update form page input invalid width form in display",
  "keyword": "When "
});
formatter.step({
  "line": 96,
  "name": "I see message error with create form",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_access_update_form_page_input_invalid_Width_form_in_display()"
});
formatter.result({
  "duration": 4012069688,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//a[@title\u003d\u0027Chi tiết form\u0027]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:563)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:556)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElementToClick(WebDriverAction.java:191)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_access_update_form_page_input_invalid_Width_form_in_display(BizwebForm.java:558)\r\n\tat ✽.When I access update form page input invalid width form in display(BizwebForm.feature:95)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_see_message_error_with_create_form()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 98,
  "name": "Update form success when input width value 8 and tick display in texbox",
  "description": "",
  "id": "bizwebform;update-form-success-when-input-width-value-8-and-tick-display-in-texbox",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 99,
  "name": "I access update form page when tick display in texbox",
  "keyword": "When "
});
formatter.step({
  "line": 100,
  "name": "I see message update success",
  "keyword": "Then "
});
formatter.step({
  "line": 101,
  "name": "I see form display in from list when update",
  "keyword": "And "
});
formatter.match({
  "location": "BizwebForm.i_access_update_form_page_when_tick_display_in_texbox()"
});
formatter.result({
  "duration": 6008030922,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//iframe[contains(@class,\u0027app-iframe\u0027)]}\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.getWrappedElement(Unknown Source)\r\n\tat org.openqa.selenium.remote.internal.WebElementToJsonConverter.apply(WebElementToJsonConverter.java:48)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver$RemoteTargetLocator.frame(RemoteWebDriver.java:879)\r\n\tat bizweb.test.actions.WebDriverAction.switchToIframe(WebDriverAction.java:87)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_access_update_form_page_when_tick_display_in_texbox(BizwebForm.java:577)\r\n\tat ✽.When I access update form page when tick display in texbox(BizwebForm.feature:99)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_see_message_success()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.form_Display_in_from_list_when_update()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 103,
  "name": "Display in frontend success",
  "description": "",
  "id": "bizwebform;display-in-frontend-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 104,
  "name": "I click icon embedded code",
  "keyword": "When "
});
formatter.step({
  "line": 105,
  "name": "I copy embedded code and paste in templace",
  "keyword": "And "
});
formatter.step({
  "line": 106,
  "name": "I click display frontend page",
  "keyword": "And "
});
formatter.step({
  "line": 107,
  "name": "I see form display in frontend",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_Click_icon_Embedded_code()"
});
formatter.result({
  "duration": 4016744378,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//a[@title\u003d\u0027Mã nhúng\u0027]}\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:563)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:556)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElementToClick(WebDriverAction.java:191)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_Click_icon_Embedded_code(BizwebForm.java:108)\r\n\tat ✽.When I click icon embedded code(BizwebForm.feature:104)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_copy_Embedded_code_and_paste_in_Templace()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.display_Frontend_Page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_see_form_display_in_frontend()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 109,
  "name": "I send information frontend",
  "description": "",
  "id": "bizwebform;i-send-information-frontend",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 110,
  "name": "I input data at frontend",
  "keyword": "When "
});
formatter.step({
  "line": 111,
  "name": "I see message notification",
  "keyword": "Then "
});
formatter.step({
  "line": 112,
  "name": "I access list form page",
  "keyword": "And "
});
formatter.step({
  "line": 113,
  "name": "I see display times send in list form page",
  "keyword": "And "
});
formatter.step({
  "line": 114,
  "name": "I see display information in list send page",
  "keyword": "And "
});
formatter.match({
  "location": "BizwebForm.i_input_Data()"
});
formatter.result({
  "duration": 4007261571,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//*[@id\u003d\u0027Data_0__Result\u0027]}\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElement(WebDriverAction.java:187)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_input_Data(BizwebForm.java:430)\r\n\tat ✽.When I input data at frontend(BizwebForm.feature:110)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_see_Message_notification()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_access_List_form_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.display_times_send_in_List_form_Page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.display_information_in_List_send_Page()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 116,
  "name": "View list send at form success",
  "description": "",
  "id": "bizwebform;view-list-send-at-form-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 117,
  "name": "I click icon view details",
  "keyword": "When "
});
formatter.step({
  "line": 118,
  "name": "I see display view details list send",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_click_icon_view_details()"
});
formatter.result({
  "duration": 4010632342,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//tr[1]/td[7]/a[1]/i}\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElement(WebDriverAction.java:187)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_click_icon_view_details(BizwebForm.java:480)\r\n\tat ✽.When I click icon view details(BizwebForm.feature:117)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.display_view_details_List_send()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 120,
  "name": "Delete list send success",
  "description": "",
  "id": "bizwebform;delete-list-send-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 121,
  "name": "I access list send page",
  "keyword": "Given "
});
formatter.step({
  "line": 122,
  "name": "I click icon delete and delete success",
  "keyword": "When "
});
formatter.step({
  "line": 123,
  "name": "I see display message",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_am_on_List_send_Page()"
});
formatter.result({
  "duration": 4009657291,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//*[@id\u003d\u0027content\u0027]/div/div[2]/div/a}\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:563)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:556)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElementToClick(WebDriverAction.java:191)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_am_on_List_send_Page(BizwebForm.java:492)\r\n\tat ✽.Given I access list send page(BizwebForm.feature:121)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_click_icon_Delete_and_Delete_success()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_see_Display_Message()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 125,
  "name": "Delete form success",
  "description": "",
  "id": "bizwebform;delete-form-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 126,
  "name": "I perform back list form",
  "keyword": "When "
});
formatter.step({
  "line": 127,
  "name": "I perform a delete form",
  "keyword": "And "
});
formatter.step({
  "line": 128,
  "name": "I see display message delete form",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.back_List_form()"
});
formatter.result({
  "duration": 4009960925,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver$RemoteTargetLocator.defaultContent(RemoteWebDriver.java:897)\r\n\tat bizweb.test.actions.WebDriverAction.exitIframe(WebDriverAction.java:91)\r\n\tat bizweb.test.stepsdefine.BizwebForm.back_List_form(BizwebForm.java:300)\r\n\tat ✽.When I perform back list form(BizwebForm.feature:126)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_Perform_a_delete_form()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_see_Display_Message_delete_form()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 130,
  "name": "Create form unsuccess when blank notification send form success",
  "description": "",
  "id": "bizwebform;create-form-unsuccess-when-blank-notification-send-form-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 131,
  "name": "I access create form page",
  "keyword": "When "
});
formatter.step({
  "line": 132,
  "name": "Create form unsuccess when blank notification send form success",
  "keyword": "And "
});
formatter.step({
  "line": 133,
  "name": "I see message error with create form",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_access_Create_form_page()"
});
formatter.result({
  "duration": 4014781445,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//a[contains(.,\u0027Tạo form\u0027)]}\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:563)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:556)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElementToClick(WebDriverAction.java:191)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_access_Create_form_page(BizwebForm.java:47)\r\n\tat ✽.When I access create form page(BizwebForm.feature:131)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.create_form_Unsuccess_when_blank_notification_send_form_success()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_see_message_error_with_create_form()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 135,
  "name": "Create form success when input new notification send form success",
  "description": "",
  "id": "bizwebform;create-form-success-when-input-new-notification-send-form-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 136,
  "name": "I create form success when input new notification send form success",
  "keyword": "When "
});
formatter.step({
  "line": 137,
  "name": "I see message create success",
  "keyword": "Then "
});
formatter.step({
  "line": 138,
  "name": "I see form display in from list when create",
  "keyword": "And "
});
formatter.match({
  "location": "BizwebForm.i_Create_form_Success_when_input_new_notification_send_form_success()"
});
formatter.result({
  "duration": 4013851297,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//iframe[contains(@class,\u0027app-iframe\u0027)]}\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.getWrappedElement(Unknown Source)\r\n\tat org.openqa.selenium.remote.internal.WebElementToJsonConverter.apply(WebElementToJsonConverter.java:48)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver$RemoteTargetLocator.frame(RemoteWebDriver.java:879)\r\n\tat bizweb.test.actions.WebDriverAction.switchToIframe(WebDriverAction.java:87)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_Create_form_Success_when_input_new_notification_send_form_success(BizwebForm.java:629)\r\n\tat ✽.When I create form success when input new notification send form success(BizwebForm.feature:136)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_see_message_success_when_enter_name()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.form_Display_in_from_list_when_create()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 140,
  "name": "Update form unsuccess when choose have and click save",
  "description": "",
  "id": "bizwebform;update-form-unsuccess-when-choose-have-and-click-save",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 141,
  "name": "I update form unsuccess when choose have and click save",
  "keyword": "When "
});
formatter.step({
  "line": 142,
  "name": "I see message error with create form",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_update_form_unSuccess_when_choose_have_and_click_Save()"
});
formatter.result({
  "duration": 4009924146,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//a[@title\u003d\u0027Chi tiết form\u0027]}\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:563)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:556)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElementToClick(WebDriverAction.java:191)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_update_form_unSuccess_when_choose_have_and_click_Save(BizwebForm.java:641)\r\n\tat ✽.When I update form unsuccess when choose have and click save(BizwebForm.feature:141)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_see_message_error_with_create_form()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 144,
  "name": "Update form success when choose have,enter correct the remaining values and click save",
  "description": "",
  "id": "bizwebform;update-form-success-when-choose-have,enter-correct-the-remaining-values-and-click-save",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 145,
  "name": "update form success when choose have enter correct and click save",
  "keyword": "When "
});
formatter.step({
  "line": 146,
  "name": "I see message update success",
  "keyword": "Then "
});
formatter.step({
  "line": 147,
  "name": "I see form display in from list when update",
  "keyword": "And "
});
formatter.match({
  "location": "BizwebForm.update_form_Success_when_choose_have_Enter_correct_and_click_Save()"
});
formatter.result({
  "duration": 4009481952,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//iframe[contains(@class,\u0027app-iframe\u0027)]}\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.getWrappedElement(Unknown Source)\r\n\tat org.openqa.selenium.remote.internal.WebElementToJsonConverter.apply(WebElementToJsonConverter.java:48)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver$RemoteTargetLocator.frame(RemoteWebDriver.java:879)\r\n\tat bizweb.test.actions.WebDriverAction.switchToIframe(WebDriverAction.java:87)\r\n\tat bizweb.test.stepsdefine.BizwebForm.update_form_Success_when_choose_have_Enter_correct_and_click_Save(BizwebForm.java:661)\r\n\tat ✽.When update form success when choose have enter correct and click save(BizwebForm.feature:145)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_see_message_success()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.form_Display_in_from_list_when_update()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 149,
  "name": "Copy embedded code success",
  "description": "",
  "id": "bizwebform;copy-embedded-code-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 150,
  "name": "I click icon embedded code",
  "keyword": "When "
});
formatter.step({
  "line": 151,
  "name": "I copy embedded code and paste in templace",
  "keyword": "And "
});
formatter.step({
  "line": 152,
  "name": "I click display frontend page",
  "keyword": "And "
});
formatter.step({
  "line": 153,
  "name": "I see form display in frontend",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_Click_icon_Embedded_code()"
});
formatter.result({
  "duration": 4015629483,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//a[@title\u003d\u0027Mã nhúng\u0027]}\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:563)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:556)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElementToClick(WebDriverAction.java:191)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_Click_icon_Embedded_code(BizwebForm.java:108)\r\n\tat ✽.When I click icon embedded code(BizwebForm.feature:150)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_copy_Embedded_code_and_paste_in_Templace()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.display_Frontend_Page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_see_form_display_in_frontend()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 155,
  "name": "I send information frontend",
  "description": "",
  "id": "bizwebform;i-send-information-frontend",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 156,
  "name": "I input data at frontend",
  "keyword": "When "
});
formatter.step({
  "line": 157,
  "name": "I see message notification when update",
  "keyword": "Then "
});
formatter.step({
  "line": 158,
  "name": "Receive email when send form",
  "keyword": "And "
});
formatter.step({
  "line": 159,
  "name": "I access list form page",
  "keyword": "And "
});
formatter.step({
  "line": 160,
  "name": "I see display times send in list form page",
  "keyword": "And "
});
formatter.step({
  "line": 161,
  "name": "I see display information in list send page",
  "keyword": "And "
});
formatter.match({
  "location": "BizwebForm.i_input_Data()"
});
formatter.result({
  "duration": 4008873400,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//*[@id\u003d\u0027Data_0__Result\u0027]}\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElement(WebDriverAction.java:187)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_input_Data(BizwebForm.java:430)\r\n\tat ✽.When I input data at frontend(BizwebForm.feature:156)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_see_Message_notification_when_update()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.receive_email_when_send_form()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_access_List_form_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.display_times_send_in_List_form_Page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.display_information_in_List_send_Page()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 163,
  "name": "View list send at form success",
  "description": "",
  "id": "bizwebform;view-list-send-at-form-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 164,
  "name": "I click icon view details",
  "keyword": "When "
});
formatter.step({
  "line": 165,
  "name": "I see display view details list send",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_click_icon_view_details()"
});
formatter.result({
  "duration": 4008580457,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//tr[1]/td[7]/a[1]/i}\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElement(WebDriverAction.java:187)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_click_icon_view_details(BizwebForm.java:480)\r\n\tat ✽.When I click icon view details(BizwebForm.feature:164)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.display_view_details_List_send()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 167,
  "name": "Delete List send success",
  "description": "",
  "id": "bizwebform;delete-list-send-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 168,
  "name": "I access list send page",
  "keyword": "Given "
});
formatter.step({
  "line": 169,
  "name": "I click icon delete and delete success",
  "keyword": "When "
});
formatter.step({
  "line": 170,
  "name": "I see display message",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_am_on_List_send_Page()"
});
formatter.result({
  "duration": 4011659995,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\n*** Element info: {Using\u003dxpath, value\u003d//*[@id\u003d\u0027content\u0027]/div/div[2]/div/a}\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:352)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:449)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:357)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:344)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:59)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:37)\r\n\tat com.sun.proxy.$Proxy16.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:227)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:39)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:213)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:210)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:563)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$18.apply(ExpectedConditions.java:556)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:208)\r\n\tat bizweb.test.actions.WebDriverAction.waitElementToClick(WebDriverAction.java:191)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_am_on_List_send_Page(BizwebForm.java:492)\r\n\tat ✽.Given I access list send page(BizwebForm.feature:168)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_click_icon_Delete_and_Delete_success()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_see_Display_Message()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 172,
  "name": "Delete form success",
  "description": "",
  "id": "bizwebform;delete-form-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 173,
  "name": "I perform back list form",
  "keyword": "When "
});
formatter.step({
  "line": 174,
  "name": "I perform a delete form",
  "keyword": "And "
});
formatter.step({
  "line": 175,
  "name": "I see display message delete form",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.back_List_form()"
});
formatter.result({
  "duration": 4012186438,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver$RemoteTargetLocator.defaultContent(RemoteWebDriver.java:897)\r\n\tat bizweb.test.actions.WebDriverAction.exitIframe(WebDriverAction.java:91)\r\n\tat bizweb.test.stepsdefine.BizwebForm.back_List_form(BizwebForm.java:300)\r\n\tat ✽.When I perform back list form(BizwebForm.feature:173)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_Perform_a_delete_form()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_see_Display_Message_delete_form()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 177,
  "name": "Logout account at googledrive success",
  "description": "",
  "id": "bizwebform;logout-account-at-googledrive-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 178,
  "name": "I access tab window  googledrive",
  "keyword": "Given "
});
formatter.step({
  "line": 179,
  "name": "I logout account success",
  "keyword": "When "
});
formatter.step({
  "line": 180,
  "name": "I see message googledrive is logout",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_access_tab_Window_GoogleDrive()"
});
formatter.result({
  "duration": 4006725720,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 4.01 seconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.executeScript(RemoteWebDriver.java:508)\r\n\tat bizweb.test.actions.WebDriverAction.openNewTabWindow(WebDriverAction.java:279)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_access_tab_Window_GoogleDrive(BizwebForm.java:696)\r\n\tat ✽.Given I access tab window  googledrive(BizwebForm.feature:178)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_logout_account_success()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "BizwebForm.i_see_message_GoogleDrive_is_logout()"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "line": 183,
  "name": "Remove app success",
  "description": "",
  "id": "bizwebform;remove-app-success",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 184,
  "name": "I remove app success",
  "keyword": "When "
});
formatter.step({
  "line": 185,
  "name": "I do not see settings page of app",
  "keyword": "Then "
});
formatter.match({
  "location": "BizwebForm.i_remove_app_success()"
});
formatter.result({
  "duration": 250246222,
  "error_message": "org.openqa.selenium.WebDriverException: chrome not reachable\n  (Session info: chrome\u003d60.0.3112.90)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 249 milliseconds\nBuild info: version: \u00272.45.0\u0027, revision: \u00275017cb8e7ca8e37638dc3091b2440b90a1d8686f\u0027, time: \u00272015-02-27 09:10:26\u0027\nSystem info: host: \u0027HaiTV-PC\u0027, ip: \u0027192.168.31.31\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_131\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\admin\\AppData\\Local\\Temp\\scoped_dir11572_28876}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d60.0.3112.90, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 33843565c39d7c930c999a4b68535956\r\n\tat sun.reflect.GeneratedConstructorAccessor22.newInstance(Unknown Source)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(Unknown Source)\r\n\tat java.lang.reflect.Constructor.newInstance(Unknown Source)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:204)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:156)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:599)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:622)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.getWindowHandles(RemoteWebDriver.java:477)\r\n\tat bizweb.test.actions.Base.unInstallApp(Base.java:41)\r\n\tat bizweb.test.stepsdefine.BizwebForm.i_remove_app_success(BizwebForm.java:715)\r\n\tat ✽.When I remove app success(BizwebForm.feature:184)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "BizwebForm.i_do_not_see_Settings_page_of_app()"
});
formatter.result({
  "status": "skipped"
});
});