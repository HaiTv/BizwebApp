Feature: BizwebForm 
I would like to collect customer information by creating a survey form on my website.

Scenario: Install app success
When I install app success
Then I see form display in app page

Scenario: Create form unsuccess when do not input name form
Given I am on list form page
When I access create form page
And I perform create form unsuccess
Then I see message error with create form

Scenario: Create form success when input name form
When I perform input data
Then I see message create success
And I see form display in from list when create

Scenario: Update form success when input new name form
When I update form success when enter new name
Then I see message update success
And I see form display in from list when update

Scenario: Copy embedded code success
When I click icon embedded code
And I copy embedded code and paste in templace
And I click display frontend page
Then I do not see form display in frontend

Scenario: Display list form success
When I access list form page 

Scenario: Display list send form empty
When I access list send form
Then I see list send form empty

Scenario: Delete form success
When I perform back list form
And I perform a delete form
Then I see display message delete form

Scenario: Create form success when pull all information feild
When I access create form page
And I perform create form success with pull all information feild
Then I see message create success
And I see form display in from list when create

Scenario: Update form success when choose required state
When I update form success when choose required state
Then I see message update success
And I see form display in from list when update
 
Scenario: Display in frontend success
When I click icon embedded code
And I copy embedded code and paste in templace
And I click display frontend page
Then I see form display in frontend

Scenario: I check required state success
When I click submit
Then I see message error

Scenario: I send information frontend
When I input data at frontend
Then I see message notification
And I access list form page 
And I see display times send in list form page
And I see display information in list send page
  
Scenario: View list send at form success 
When I click icon view details
Then I see display view details list send
  
Scenario: Delete list send success
Given I access list send page
When I click icon delete and delete success
Then I see display message
 
Scenario: Delete form success
When I perform back list form
And I perform a delete form
Then I see display message delete form

Scenario: Create form unsuccess when do not customize content
When I access create form page
And I create form unsuccess when do not customize content
Then I see message error with create form

Scenario: Create form success when input new customize content
When I create form success when input new customize content
Then I see message create success
And I see form display in from list when create

Scenario: Update form unsuccess when input width form in display > 720px value 8
When I access update form page input invalid width form in display
Then I see message error with create form

Scenario: Update form success when input width value 8 and tick display in texbox
When I access update form page when tick display in texbox
Then I see message update success
And I see form display in from list when update

Scenario: Display in frontend success
When I click icon embedded code
And I copy embedded code and paste in templace
And I click display frontend page
Then I see form display in frontend

Scenario: I send information frontend
When I input data at frontend
Then I see message notification
And I access list form page 
And I see display times send in list form page
And I see display information in list send page

Scenario: View list send at form success
When I click icon view details
Then I see display view details list send

Scenario: Delete list send success
Given I access list send page
When I click icon delete and delete success
Then I see display message

Scenario: Delete form success
When I perform back list form
And I perform a delete form
Then I see display message delete form

Scenario: Create form unsuccess when blank notification send form success
When I access create form page
And Create form unsuccess when blank notification send form success
Then I see message error with create form

Scenario: Create form success when input new notification send form success 
When I create form success when input new notification send form success 
Then I see message create success
And I see form display in from list when create

Scenario: Update form unsuccess when choose have and click save 
When I update form unsuccess when choose have and click save 
Then I see message error with create form

Scenario: Update form success when choose have,enter correct the remaining values and click save 
When  update form success when choose have enter correct and click save
Then I see message update success
And I see form display in from list when update 

Scenario: Copy embedded code success
When I click icon embedded code
And I copy embedded code and paste in templace
And I click display frontend page
Then I see form display in frontend

Scenario: I send information frontend
When I input data at frontend
Then I see message notification when update
And Receive email when send form
And I access list form page 
And I see display times send in list form page
And I see display information in list send page

Scenario: View list send at form success
When I click icon view details
Then I see display view details list send

Scenario: Delete List send success
Given I access list send page
When I click icon delete and delete success
Then I see display message

Scenario: Delete form success
When I perform back list form
And I perform a delete form
Then I see display message delete form

Scenario: Logout account at googledrive success
Given I access tab window  googledrive 
When I logout account success
Then I see message googledrive is logout


Scenario: Remove app success
When I remove app success
Then I do not see settings page of app
